// ros
#include <ros/ros.h>
// ros package to access package directory
#include <ros/package.h>
// move base
#include <move_base_msgs/MoveBaseAction.h>
// simple move action
#include <actionlib/client/simple_action_client.h>
#include <actionlib/server/simple_action_server.h>
// stamped point message
#include <geometry_msgs/Pose.h>
// tf2 matrix
#include <tf2/LinearMath/Matrix3x3.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/LinearMath/Transform.h>
// yaml file handling
#include <yaml-cpp/yaml.h>
//
#include <fstream>

#include "tf/transform_listener.h"

// #include <dbot_msgs/RobotStatus.h>

#include <dbot_common/node_state.h>
#include <dbot_msgs/DbotControlAction.h>
#include <dbot_msgs/RobotState.h>
#include <nav_msgs/Path.h>
#include <visualization_msgs/MarkerArray.h>
#include <thread>

namespace dbot {

// parameter struct and typedef
struct MoveBaseParam {
  // waypoint filename
  std::string waypoints_filename;
  // reference frame. the robot wil move based on this frame
  std::string ref_frame;
  // number of runs
  int num_runs;
};

template <typename T>
void operator>>(const YAML::Node &node, T &i) {
  i = node.as<T>();
}
/*
 * Waypoint Navigation class
 */
class MoveBase {
 public:
  ///
  MoveBase(std::string name, dbot_msgs::RobotState* robot_state);
  ///
  ~MoveBase();
  ///
  void init();
  ///
  void run();
  ///
  bool buildWaypointsFromFile();

  bool buildWaypointsFromActionServer(const dbot_msgs::DbotControlGoal &goal);

  bool FinalGoalReceived(move_base_msgs::MoveBaseGoal &goal);

  void CurrentPosition();

  bool NextWaypointTimingIndicator(int current_aisle, double distance);

  void SendWaypoint(int current_aisle, move_base_msgs::MoveBaseGoal &goal);

  bool isFinalGoal(int current_aisle);

	void SetNodeState(const dbot_common::NodeState &nodestate);
	dbot_common::NodeState GetNodeState();

	void StartThread();
	void StopThread();
  void SubRobotState(const dbot_msgs::RobotState& msg);

  boost::thread thread_;
	boost::mutex state_mtx_;

  std::string source_frameid = std::string("/map");
  std::string target_frameid = std::string("/base_link");
  // tell the action client that we want to spin a thread by default
  actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> action_client;

  // Instantiate a local listener
  tf::TransformListener echoListener;

  tf::Vector3 current_pose;
  dbot_msgs::RobotState* robot_state_;

 private:
  /// ros node handle
  ros::NodeHandle nh_;
  ///
  MoveBaseParam params_;
  /// input image file name including the path
  std::string waypoints_filename_;
  /// array of waypoint
  ros::Subscriber robot_status_sub_;

  std::vector<geometry_msgs::Pose> waypoints_;

  ros::Subscriber waypoint_sub_;  //!< subscriber of manually inserted waypoints
  ros::Publisher
      waypoint_marker_pub_;  //!< publisher of waypoint visualization markers
  // flags
  bool clear_waypoints_;  //!< flag indicating that the waypoint container must
                          //!< be cleared to start anew
  bool is_running_;
  bool send_waypoints_;

  std::thread dbot_movebase_thread_;
  // dbot::MoveBase::MoveBaseAction
  actionlib::SimpleActionServer<dbot_msgs::DbotControlAction> as_;
  // defining action as a member variable.
  dbot_msgs::DbotControlGoal goal_;
  dbot_msgs::DbotControlResult result_;
  dbot_msgs::DbotControlFeedback feedback_;
  void ExecuteCB(const dbot_msgs::DbotControlGoalConstPtr &goal);

  std::string action_name_;

  dbot_common::NodeState node_state_;
  // void RobotStatusSubscriber(const dbot_msgs::RobotStatus &msg);
};

}  // namespace dbot
