#include "dbot_movebase.h"

ros::Subscriber robot_state_sub_;
dbot_msgs::RobotState robot_state;

void SubRobotState(const dbot_msgs::RobotState& msg) {
  robot_state.robot_state = msg.robot_state;
  robot_state.local_state = msg.local_state;
  return;
}

/*
 * main function
 */
int main(int argc, char **argv) {
  // ros init
  ros::init(argc, argv, "dbot_movebase_node");
  ros::NodeHandle n;

  robot_state_sub_ = n.subscribe("/robot_state", 0, SubRobotState);

  // create a waypoint navigation instance
  dbot::MoveBase movebase("dbot_movebase_action",&robot_state);

  // initialize waypoint navigation
  movebase.init();

  // spin ROS
  try {
    // ros takes all
    ros::spin();
  } catch (std::runtime_error &e) {
    ROS_ERROR("ros spin failed: %s", e.what());
    return -1;
  }
  movebase.thread_.join();

  return 0;
}