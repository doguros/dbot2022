#include "dbot_movebase.h"

namespace dbot {

MoveBase::MoveBase(std::string name, dbot_msgs::RobotState* robot_state)
    : action_client("move_base", true),
      clear_waypoints_(false),
      as_(nh_, name, boost::bind(&MoveBase::ExecuteCB, this, _1), false),
      action_name_(name) {
  as_.start();
  SetNodeState(dbot_common::NodeState::IDLE);
  robot_state_ = robot_state;
}

MoveBase::~MoveBase() {}

void MoveBase::ExecuteCB(const dbot_msgs::DbotControlGoalConstPtr &goal) {
  // helper variables
  ROS_INFO("Goal Receive");
  ros::NodeHandle nh("~");
  ros::Rate r(5);

  waypoints_ = goal->waypoints;

  if (GetNodeState() == dbot_common::NodeState::IDLE) {
    SetNodeState(dbot_common::NodeState::RUNNING);
    StartThread();
  } else {
    as_.setAborted();
  }
  // start executing the action
  while (ros::ok()) {
    if (as_.isPreemptRequested() || !ros::ok()) {
      ROS_INFO("%s: Preempted", action_name_.c_str());
      // set the action state to preempted
      as_.setPreempted();
			action_client.cancelAllGoals();
      SetNodeState(dbot_common::NodeState::IDLE);
      StopThread();
      break;
    }

    if (GetNodeState() == dbot_common::NodeState::SUCCESS) {
      SetNodeState(dbot_common::NodeState::IDLE);
      StopThread();
      ROS_INFO("Succeeded");
      as_.setSucceeded();
      break;
    }

    if (GetNodeState() == dbot_common::NodeState::FAILURE) {
      SetNodeState(dbot_common::NodeState::IDLE);
      ROS_INFO("Failed");
      StopThread();
      as_.setAborted();
      break;
    }

    r.sleep();
  }
}

void MoveBase::init() {
  // private node handle for getting parameters
  ros::NodeHandle ros_nh("~");

  // number of runs to repeat the waypoint navigation
  ros_nh.param<int>("num_loops", params_.num_runs, 1);

  // setup waypoints filename
  // input waypoint filename
  ros_nh.param<std::string>("waypoints_filename", params_.waypoints_filename,
                            "config/waypoints.yaml");
  // referece frame
  ros_nh.param<std::string>("ref_frame", params_.ref_frame, "base_link");

  is_running_ = false;

  // thread_ = boost::thread(&MoveBase::run, this);

  ROS_INFO("Wating move_base action server");
  // wait for the action server to come up
  while (!action_client.waitForServer()) {
    ROS_INFO("Waiting for the move_base action server to come up");
  }

#if __DEBUG__
  ROS_INFO("number of runs: %d", params_.num_runs);
  ROS_INFO("waypoints filename: %s", waypoints_filename_.c_str());
#endif
}

// Check if the waypoint is the final goal
bool MoveBase::isFinalGoal(int current_aisle) {
  if (current_aisle == waypoints_.size() - 1)
    return true;
  else
    return false;
}

// Calculate distance between goal and current position
double GoalDistance(double x1, double y1, double x2, double y2) {
  return sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2) * 1.0);
}

void MoveBase::SendWaypoint(int current_aisle,
                            move_base_msgs::MoveBaseGoal &goal) {
  // TODO : 함수화

  // send a goal to the robot
  goal.target_pose.header.frame_id = "map";  // params_.ref_frame;
  goal.target_pose.header.stamp = ros::Time::now();
  goal.target_pose.pose.position.x = waypoints_[current_aisle].position.x;
  goal.target_pose.pose.position.y = waypoints_[current_aisle].position.y;
  goal.target_pose.pose.position.z = 0.0;
  goal.target_pose.pose.orientation.x = waypoints_[current_aisle].orientation.x;
  goal.target_pose.pose.orientation.y = waypoints_[current_aisle].orientation.y;
  goal.target_pose.pose.orientation.z = waypoints_[current_aisle].orientation.z;
  goal.target_pose.pose.orientation.w = waypoints_[current_aisle].orientation.w;

  ROS_INFO("Sending goal: (%.2f, %.2f, %.2f)", goal.target_pose.pose.position.x,
           goal.target_pose.pose.position.y,
           goal.target_pose.pose.orientation.z);
  action_client.sendGoal(goal);
  ROS_INFO("goal sent");
}

// If the distance between goal and current position is less than
// NextWaypointDistance send the next waypoint
bool MoveBase::NextWaypointTimingIndicator(int current_aisle, double distance) {
  if (GoalDistance(waypoints_[current_aisle].position.x,
                   waypoints_[current_aisle].position.y, current_pose.getX(),
                   current_pose.getY()) < distance)
    return true;
  else
    return false;
}

void MoveBase::CurrentPosition() {
  int precision(3);
  tf::StampedTransform echo_transform;
  echoListener.waitForTransform(source_frameid, target_frameid, ros::Time(),
                                ros::Duration(1.0));
  echoListener.lookupTransform(source_frameid, target_frameid, ros::Time(),
                               echo_transform);
  std::cout.precision(precision);
  std::cout.setf(std::ios::fixed, std::ios::floatfield);

  current_pose = echo_transform.getOrigin();
}

// Wait for the result from SimpleClientGoalState
bool MoveBase::FinalGoalReceived(move_base_msgs::MoveBaseGoal &goal) {
  action_client.waitForResult(ros::Duration());
  if (action_client.getState() == actionlib::SimpleClientGoalState::SUCCEEDED) {
    ROS_INFO("Homing Succeeded");
    return true;
  } else if (action_client.getState() ==
             actionlib::SimpleClientGoalState::ACTIVE) {
    return false;
  }
}

// Run waypoint navigation
void MoveBase::run() {
  // if (waypoints_.size() <= 1) {
  //   ROS_WARN("Need more than two points");
  //   SetNodeState(dbot_common::NodeState::FAILURE);
  //   return;
  // }
  // ROS_WARN("local_state %d", robot_state_->local_state);
  // if (robot_state_->local_state < 10) {
  //   ROS_FATAL("Localization error : need to be localized again");
  //   SetNodeState(dbot_common::NodeState::PAUSE);
  // } else if (robot_state_->local_state >= 10) {
  //   SetNodeState(dbot_common::NodeState::RUNNING);
  // }
  int waypoint_idx = 0;
  CurrentPosition();
  move_base_msgs::MoveBaseGoal goal;
  SendWaypoint(waypoint_idx, goal);

  while (ros::ok() && GetNodeState() == dbot_common::NodeState::RUNNING) {
    CurrentPosition();
    // if the goal is home
    if (isFinalGoal(waypoint_idx) == true && robot_state_->robot_state == "AtoH") {
      bool is_reached = FinalGoalReceived(goal);
      if (is_reached) {
        SetNodeState(dbot_common::NodeState::SUCCESS);
        action_client.cancelAllGoals();
        return;
      }
    } else if (isFinalGoal(waypoint_idx) == true){// && robot_state_->robot_state != "PATROL") {
      bool is_reached = NextWaypointTimingIndicator(waypoint_idx, 0.5);
      if (is_reached) {
        SetNodeState(dbot_common::NodeState::SUCCESS);
        action_client.cancelAllGoals();
        return;
      }
    } else {
      bool is_reached = NextWaypointTimingIndicator(waypoint_idx, 0.5);

      if (is_reached) {
        waypoint_idx = waypoint_idx + 1;
        SendWaypoint(waypoint_idx, goal);
        // if (isFinalGoal(waypoint_idx) == true && robot_state_->robot_state == "PATROL"){
        //   waypoint_idx = 0;
        // }
      }
    }
  }
}

void MoveBase::SetNodeState(const dbot_common::NodeState &nodestate) {
  boost::lock_guard<boost::mutex> guard(state_mtx_);
  node_state_ = nodestate;
}

dbot_common::NodeState MoveBase::GetNodeState() {
  boost::lock_guard<boost::mutex> guard(state_mtx_);
  return node_state_;
}

void MoveBase::StopThread() {
  if (thread_.joinable()) {
    thread_.join();
  }
  ROS_INFO("Thread is stopped");
}

void MoveBase::StartThread() {
  if (thread_.joinable()) {
    thread_.join();
  }

  SetNodeState(dbot_common::NodeState::RUNNING);
  thread_ = boost::thread(&MoveBase::run, this);
}

}  // namespace dbot