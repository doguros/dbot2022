#ifndef PACKET_H
#define PACKET_H



namespace dbot{
   
    //Serial
    const __uint16_t SN_MIN     = 0xFF00;
    const __uint16_t SN_MAX     = 0xFFFF;

    // AorM
    // const __uint8_t MANUAL      = 0x00;
    // const __uint8_t AUTO        = 0x01;
    // const __uint8_t RESERVED    = 0x02;

    const __uint8_t FRONT      = 0x00;
    const __uint8_t BACK       = 0x01;
    

    typedef struct DataPacket{
        __uint16_t 		SN; 			// Serial No 	    	- 2 Byte
        unsigned char 	RID[6]; 		// Robot ID 	    	- 6 Byte
        __uint16_t 		TYPE;		// Robot Type 	        	- 2 Byte
        __uint8_t 		STATE;		// Robot State 		        - 1 Byte
        __uint64_t 		STATUS;		// Robot Status		        - 8 Byte
        __uint64_t 		SWITCH;		// Robot Switch		        - 8 Byte
        double 			x;			// X point		            - 8 Byte
        double 			y;			// y point		            - 8 Byte
        double 			w;			// radian		            - 8 Byte
        double          h;          // Orientation (Not Use)    - 8 Byte
        __uint8_t 		Battery1;		// Battery 1		    - 1 Byte
        __uint8_t 		Battery2;		// Battery 2 		    - 1 Byte
        __uint8_t 		MapID;		// Map ID		            - 1 Byte
        __uint8_t 		ForB;			// Front or Back	    - 1 Byte
        __uint8_t		Checksum;		// Checksum	        	- 1 Byte
    };

    typedef struct AckPacket{
        __uint16_t 		SN; 			// Serial No 		    - 2 Byte
        __uint8_t		ACK;	    	// Checksum		        - 1 Byte
    };
    
}

#endif
