#ifndef SUBSCRIBERS_H
#define SUBSCRIBERS_H

#include <ros/ros.h>

#include <QObject>

//Message
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>


namespace dbot{
class Subscribers: public QObject{
    Q_OBJECT
public:
    Subscribers();
    ~Subscribers();

private:
    ros::NodeHandle nh_;

    ros::Subscriber robot_pose_sub_;
    ros::Subscriber out_vel_sub_;

    void RobotPoseCallback(const geometry_msgs::Pose::ConstPtr& msg);
    void OutVelCallback(const geometry_msgs::Twist::ConstPtr& msg);
    
Q_SIGNALS:
    void SendPoseStatus(const geometry_msgs::Pose::ConstPtr& msg);
    void SendOutVel(const geometry_msgs::Twist::ConstPtr& msg);
};

}// namespace dbot

#endif