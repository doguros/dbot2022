#ifndef TCP_CLIENT_H
#define TCP_CLIENT_H

//QT
#include <QObject>
#include <QTcpSocket>
#include <QDataStream>
#include <QString>

//STL
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include<string>

//dbot
#include "subscribers.h"
#include "packets.h"

#define DBOT 0x01
#define SERVER_IP "172.20.10.1"
#define PORT_NUM 5555

namespace dbot{
class TCPClient : public QObject{
    Q_OBJECT
public:
    TCPClient();
    ~TCPClient();

private:
    Subscribers *subscribers_;

    int sock_;
    struct sockaddr_in serv_addr_;
    int packet_len_;

    DataPacket *packet_;

    QTcpSocket *socket_;
//    int CreateSocket();
//    int Connect();
    
    // Use Qt Library
    bool ConnectToServer();
    QByteArray PacketToArray(const DataPacket& packet);
    bool Send(QByteArray data);

    //void SetData();
    void InitData();
    //int Send(const DataPacket& packet);

private Q_SLOTS:
    void SetRobotPose(const geometry_msgs::Pose::ConstPtr& msg);
    void SetOutVel(const geometry_msgs::Twist::ConstPtr& msg);
    

};
}   // namespace dbot

#endif