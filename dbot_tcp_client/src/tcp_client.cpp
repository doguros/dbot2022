#include "tcp_client.h"
namespace dbot{
TCPClient::TCPClient(){
    subscribers_ = new Subscribers();
    
    //CreateSocket();
    
    if(!ConnectToServer()){
        ROS_INFO("Fail Connect to Server");
        return;
    }

    ROS_INFO("Success Connect to Server");
    packet_ = new DataPacket;
    InitData();

    connect(subscribers_, SIGNAL(SendPoseStatus(const geometry_msgs::Pose::ConstPtr&)), this, SLOT(SetRobotPose(const geometry_msgs::Pose::ConstPtr&)));
    connect(subscribers_, SIGNAL(SendOutVel(const geometry_msgs::Twist::ConstPtr&)), this, SLOT(SetOutVel(const geometry_msgs::Twist::ConstPtr&)));
    //example code
    // while(1){
    //     sleep(1);
    //     SetData();
    // }
};

TCPClient::~TCPClient(){

};

// int TCPClient::CreateSocket(){
//     if((sock_ = socket(AF_INET,SOCK_STREAM,0)) == -1){
//         ROS_ERROR("sock failed (error code: %d)",errno);
//         return errno;
//     }

//     memset(&serv_addr_,0,sizeof(serv_addr_));
    
//     // AF_INET - > ipv4
//     serv_addr_.sin_family       = AF_INET;
//     serv_addr_.sin_addr.s_addr  = inet_addr(SERVER_IP);
//     serv_addr_.sin_port         = htons(PORT_NUM);

//     //return Connect();
//     return 0;
// };


// int TCPClient::Connect(){
//     // ROS_INFO("Connecting...");
//     // if(connect(sock_, (struct sockaddr *)&serv_addr_,sizeof(serv_addr_)) != 0 ){
//     //     ROS_ERROR("connect failed (error code: %d)",errno);

//     //     return errno;
//     // }
//     //ROS_INFO("Connected");
    
// };
bool TCPClient::ConnectToServer(){
    socket_ = new QTcpSocket(this);

    socket_->connectToHost(SERVER_IP,PORT_NUM);
    return socket_->waitForConnected();
}

// void TCPClient::SetData(){
//     DataPacket* data_packet = new DataPacket;

//     // Serial Number
//     data_packet->SN = 0x0000;

//     //Robot ID
//     data_packet->RID[0] = 'G';
//     data_packet->RID[1] = 'S';
//     data_packet->RID[2] = 'E';
//     data_packet->RID[3] = '0';
//     data_packet->RID[4] = '0';
//     data_packet->RID[5] = '1';

//     // Robot Type
//     data_packet->TYPE = 0;
//     // Robot State
//     data_packet->STATE = 0;
//     // Robot Status
//     data_packet->STATUS = 0;
//     // Robot Switch
//     data_packet->SWITCH = 0;

//     data_packet->x = 0.0;
//     data_packet->y = 0.0;
//     data_packet->w = 0.0;
//     data_packet->h = 0.0;   // Not use
    
//     // Battery
//     data_packet->Battery1 = 0;
//     data_packet->Battery2 = 0;
    
//     data_packet->MapID = 0; // Not use
    
//     // Mode
//     data_packet->AorM = MANUAL; // or AUTO or RESERVED
    
//     //return data_packet;
// };

void TCPClient::InitData(){
    
    // Serial Number
    packet_->SN = 0x0000;

    //Robot ID
    packet_->RID[0] = 'G';
    packet_->RID[1] = 'S';
    packet_->RID[2] = 'E';
    packet_->RID[3] = '0';
    packet_->RID[4] = '0';
    packet_->RID[5] = '1';

    // Robot Type
    packet_->TYPE = 0;
    // Robot State
    packet_->STATE = 0;
    // Robot Status
    packet_->STATUS = 0;
    // Robot Switch
    packet_->SWITCH = 0;

    packet_->x = 0.0;
    packet_->y = 0.0;
    packet_->w = 0.0;
    packet_->h = 0.0;
    
    // Battery
    packet_->Battery1 = 0;
    packet_->Battery2 = 0;
    
    packet_->MapID = 0; // Not use
    
    // Mode
    packet_->ForB = FRONT; // or FRONT or BACK
    
}

// int TCPClient::Send(const DataPacket& packet){

//     packet_len_ = sizeof(DataPacket);
    
//     ROS_INFO("SN size: %d", sizeof(packet.SN));
//     ROS_INFO("RID size: %d", sizeof(packet.RID));
//     ROS_INFO("Type size: %d", sizeof(packet.TYPE));
//     ROS_INFO("State size: %d", sizeof(packet.STATE));
//     ROS_INFO("Status size: %d", sizeof(packet.STATUS));
//     ROS_INFO("Switch size: %d", sizeof(packet.SWITCH));
//     ROS_INFO("x size: %d", sizeof(packet.x));
//     ROS_INFO("y size: %d", sizeof(packet.y));
//     ROS_INFO("w size: %d", sizeof(packet.w));
//     ROS_INFO("h size: %d", sizeof(packet.h));
//     ROS_INFO("Battery1 size: %d", sizeof(packet.Battery1));
//     ROS_INFO("Battery2 size: %d", sizeof(packet.Battery2));

//     ROS_INFO("MapID size: %d", sizeof(packet.MapID));
//     ROS_INFO("AorM size: %d", sizeof(packet.AorM));
//     ROS_INFO("Checksum size: %d", sizeof(packet.Checksum));

//     ROS_INFO("Packet size=%d", sizeof(packet));

//     if(write(sock_, &packet, sizeof(packet)) <= 0){
//         ROS_ERROR("write failed (error code: %d)",errno);

//         return 1;
//     }

//     return 0;

// };

bool TCPClient::Send(QByteArray data){
    socket_->write(data);

    return socket_->waitForBytesWritten();
}

QByteArray TCPClient::PacketToArray(const DataPacket& packet){
    
    QByteArray array;
    QDataStream data(&array, QIODevice::ReadWrite);
    
    // data << packet.RID[0];
    // data << packet.RID[1];
    // data << packet.RID[2];
    // data << packet.RID[3];
    // data << packet.RID[4];
    // data << packet.RID[5];

    // Example code double -> string
    std::stringstream ssDouble;
    ssDouble << packet.x;
    data << ssDouble.str().c_str();

    return array;
}

void TCPClient::SetRobotPose(const geometry_msgs::Pose::ConstPtr& msg){
    ROS_INFO("Get RobotPose");

    packet_->x = msg->position.x;
    packet_->y = msg->position.y;

    socket_->write(PacketToArray(*packet_));
    
    if(!socket_->waitForBytesWritten()){
        ROS_INFO("Failed Write");
    }

};

void TCPClient::SetOutVel(const geometry_msgs::Twist::ConstPtr& msg){
    ROS_INFO("Get out_vel");

    if(msg->linear.x < 0 ){
        packet_->ForB = 0;
    }else
    {
        packet_->ForB = 1;
    }
    packet_->w = msg->angular.z;
    
    socket_->write(PacketToArray(*packet_));

    if(!socket_->waitForBytesWritten()){
        ROS_INFO("Failed Write");
    }

};
}   //namespace dbot 
