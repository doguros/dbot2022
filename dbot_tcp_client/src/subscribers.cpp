
#include "subscribers.h"

namespace dbot{
Subscribers::Subscribers():
    nh_("~")    
{
    robot_pose_sub_ = nh_.subscribe("/robot_pose", 1000, &Subscribers::RobotPoseCallback, this);
    out_vel_sub_ = nh_.subscribe("/out_vel", 1000, &Subscribers::OutVelCallback, this);
};

Subscribers::~Subscribers(){

};


void Subscribers::RobotPoseCallback(const geometry_msgs::Pose::ConstPtr& msg){
    SendPoseStatus(msg);
};

void Subscribers::OutVelCallback(const geometry_msgs::Twist::ConstPtr& msg){
    SendOutVel(msg);
};


} //namespace dbot