#include <ros/ros.h>

#include "tcp_client.h"

int main(int argc, char **argv){

     if( !ros::isInitialized() )
  {
    ros::init( argc, argv, "dbot_tcp_client", ros::init_options::AnonymousName );
  }

  dbot::TCPClient* tcp_client = new dbot::TCPClient();

  ros::spin();
  return 0;
}
