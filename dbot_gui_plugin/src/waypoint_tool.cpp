/*
 */

// Ogre
#include <OGRE/OgreEntity.h>
#include <OGRE/OgreSceneManager.h>
#include <OGRE/OgreSceneNode.h>

// ROS
#include <ros/console.h>

// Rviz
#include <rviz/geometry.h>
#include <rviz/mesh_loader.h>
#include <rviz/ogre_helpers/shape.h>
#include <rviz/properties/vector_property.h>
#include <rviz/viewport_mouse_event.h>

// This package
#include "waypoint_tool.h"

namespace dbot {

WaypointTool::WaypointTool()
    : moving_waypoint_node_(NULL),
      current_waypoint_property_(NULL),
      clicked_waypoint_property_(NULL) {}

WaypointTool::~WaypointTool() {
  for (std::size_t i = 0; i < waypoint_nodes_->size(); i++) {
    scene_manager_->destroySceneNode(waypoint_nodes_->at(i));
  }
  
  state_ = State::Deactivate;
  StopThread();
}

void WaypointTool::onInitialize() {
  moving_waypoint_node_ =
      scene_manager_->getRootSceneNode()->createChildSceneNode();
  AttachWaypointShape(moving_waypoint_node_);
  moving_waypoint_node_->setVisible(false);

  waypoint_nodes_ = new std::vector<Ogre::SceneNode*>;

  move_tool_.initialize(context_);
  nh_ = ros::NodeHandle("~");
  sub_pose_ =
      nh_.subscribe("/robot_pose", 10, &WaypointTool::RobotPoseUpdate, this);
  state_ = State::Deactivate;
}

void WaypointTool::activate() {
  if (moving_waypoint_node_) {
    ROS_INFO("Activate_if");
    current_waypoint_property_ = new rviz::VectorProperty("Waypoint");
    current_waypoint_property_->setReadOnly(false);
    getPropertyContainer()->addChild(current_waypoint_property_, 0);
  }

  state_ = State::Activate;
  StartThread();
}

void WaypointTool::deactivate() {
  if (moving_waypoint_node_) {
    moving_waypoint_node_->setVisible(false);
    delete current_waypoint_property_;
    current_waypoint_property_ = NULL;
  }
  state_ = State::Deactivate;
  StopThread();
}

int WaypointTool::processMouseEvent(rviz::ViewportMouseEvent& event) {
  if (event.control()) {
    if (!moving_waypoint_node_) {
      return Render;
    }

    Ogre::Vector3 intersection;
    Ogre::Plane ground_plane(Ogre::Vector3::UNIT_Z, 0.0f);

    if (rviz::getPointOnPlaneFromWindowXY(event.viewport, ground_plane, event.x,
                                          event.y, intersection)) {
      moving_waypoint_node_->setVisible(true);
      moving_waypoint_node_->setPosition(intersection);
      current_waypoint_property_->setVector(intersection);

      if (event.leftDown()) {
        MakeWaypoint(intersection);

        clicked_waypoint_property_ = NULL;

        return Render;
      } else if (event.rightDown()) {
        if (waypoint_nodes_->empty()) {
          return Render;
        }

        if (event.shift()) {
          // Ctrl + Shift + rightDown
          RemoveAllWaypoint();
        } else {
          RemoveWaypoint(waypoint_nodes_->size() - 1);
        }
      }  // Logic for clicked button of Mouse

    } else {
      moving_waypoint_node_->setVisible(false);
    }

    return Render;
  } else {
    moving_waypoint_node_->setVisible(false);
    move_tool_.processMouseEvent(event);
    setCursor(move_tool_.getCursor());
    return 0;
  }
}

std::vector<Ogre::SceneNode*>* WaypointTool::GetWaypoints() {
  return waypoint_nodes_;
}

void WaypointTool::RemoveAllWaypoint() {
  size_t num_waypoint = waypoint_nodes_->size();
  for (size_t i = 0; i < num_waypoint; i++) {
    getPropertyContainer()->removeChildren(waypoint_nodes_->size());
    RemoveLine(waypoint_nodes_->size() - 1);
    Ogre::SceneNode* node = waypoint_nodes_->back();
    scene_manager_->destroySceneNode(node);
    waypoint_nodes_->pop_back();
  }

  WaypointUpdated();
}

void WaypointTool::RemoveLastWaypoint() {
  getPropertyContainer()->removeChildren(waypoint_nodes_->size());
  RemoveLine(waypoint_nodes_->size() - 1);
  Ogre::SceneNode* node = waypoint_nodes_->back();
  scene_manager_->destroySceneNode(node);
  waypoint_nodes_->pop_back();

  WaypointUpdated();
}

void WaypointTool::RemoveWaypoint(int idx) {
  scene_manager_->destroySceneNode(waypoint_nodes_->at(idx));
  waypoint_nodes_->erase(waypoint_nodes_->begin() + idx);
  getPropertyContainer()->removeChildren(idx + 1);
  RemoveLine(idx);

  WaypointUpdated();
}

void WaypointTool::MakeWaypoint(const Ogre::Vector3& point) {
  if (waypoint_nodes_->size() == 0) {
    MakeCurrentPoseToWaypoint();
  }

  clicked_waypoint_property_ = new rviz::VectorProperty(
      "Waypoint" + QString::number(waypoint_nodes_->size()));

  clicked_waypoint_property_->setVector(point);
  getPropertyContainer()->addChild(clicked_waypoint_property_);

  Ogre::SceneNode* node =
      scene_manager_->getRootSceneNode()->createChildSceneNode();
  AttachWaypointShape(node);
  node->setVisible(true);
  node->setPosition(point);
  waypoint_nodes_->push_back(node);

  AddLine(waypoint_nodes_->size() - 2, waypoint_nodes_->size() - 1);

  WaypointUpdated();
}

void WaypointTool::MakeCurrentPoseToWaypoint() {
  Ogre::Vector3 point;
  point.x = robot_pose_.position.x;
  point.y = robot_pose_.position.y;
  point.z = robot_pose_.position.z;

  clicked_waypoint_property_ = new rviz::VectorProperty(
      "Waypoint" + QString::number(waypoint_nodes_->size()));
  clicked_waypoint_property_->setVector(point);
  getPropertyContainer()->addChild(clicked_waypoint_property_);

  Ogre::SceneNode* node =
      scene_manager_->getRootSceneNode()->createChildSceneNode();
  AttachWaypointShape(node);
  node->setVisible(true);
  node->setPosition(point);
  waypoint_nodes_->push_back(node);
}

void WaypointTool::RemoveLine(int idx) {
  if (idx <= 0) {
    return;
  }
  rviz::Line* line = line_nodes_.at(idx - 1);
  delete line;
  line_nodes_.erase(line_nodes_.begin() + idx - 1);
}

void WaypointTool::AddLine(int idx_src, int idx_dst) {
  rviz::Line* line = new rviz::Line(scene_manager_);

  Ogre::SceneNode* src_node = waypoint_nodes_->at(idx_src);
  Ogre::SceneNode* dst_node = waypoint_nodes_->at(idx_dst);

  line->setPoints(src_node->getPosition(), dst_node->getPosition());
  line->setVisible(true);

  line_nodes_.push_back(line);
}

void WaypointTool::AttachWaypointShape(Ogre::SceneNode* node) {
  rviz::Shape* shape_ =
      new rviz::Shape(rviz::Shape::Cylinder, scene_manager_, node);
  shape_->setOrientation(Ogre::Quaternion(0, 0, 1, 1));
  shape_->setScale(Ogre::Vector3(0.3, 0.001, 0.3));
  shape_->setColor(0.0, 1, 0.0, 1);
}

void WaypointTool::UpdateWaypoint(int idx, double x, double y) {}

void WaypointTool::StartThread() {
  if (pose_subcribe_thread_.joinable()) {
    pose_subcribe_thread_.join();
  }

  pose_subcribe_thread_ =
      boost::thread(&WaypointTool::SubscribeRobotPose, this);
}
void WaypointTool::StopThread() {
  if (pose_subcribe_thread_.joinable()) {
    pose_subcribe_thread_.join();
  }
}

void WaypointTool::RobotPoseUpdate(const geometry_msgs::Pose::ConstPtr& msg) {
  robot_pose_.position.x = msg->position.x;
  robot_pose_.position.y = msg->position.y;
}

void WaypointTool::SubscribeRobotPose() {
  ros::Rate rate(20);
  while (ros::ok() && state_ == State::Activate) {
    rate.sleep();
  }
}

}  // namespace dbot

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(dbot::WaypointTool, rviz::Tool)