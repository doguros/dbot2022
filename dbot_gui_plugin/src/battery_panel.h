#ifndef BATTERY_PANEL_H
#define BATTERY_PANEL_H

#include <QWidget>

#include <rviz/panel.h>
#include <ros/ros.h>
#include <ros/message.h>

#include "dbot_msgs/RobotStatus.h"
namespace Ui {
class BatteryPanel;
}

namespace dbot{
class BatteryPanel : public rviz::Panel
{
    Q_OBJECT

public:
    explicit BatteryPanel(QWidget *parent = 0);
    ~BatteryPanel();

private:
    Ui::BatteryPanel *ui;

    ros::NodeHandle nh_;
    ros::Subscriber battery_sub_;

    void BatteryCallback(const dbot_msgs::RobotStatus::ConstPtr& msg);

private Q_SLOTS:
    void SetBattery(int battery);
Q_SIGNALS:
    void ReadBattery(int battery);
};

}
#endif // BATTERY_PANEL_H
