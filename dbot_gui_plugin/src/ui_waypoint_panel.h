/********************************************************************************
** Form generated from reading UI file 'waypoint_panel.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WAYPOINT_PANEL_H
#define UI_WAYPOINT_PANEL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WaypointPanel {
 public:
  QGridLayout *gridLayout_2;
  QGridLayout *waypointPanelLayout;
  QTableWidget *waypointTableWidget;
  QPushButton *resetButton;
  QPushButton *createOnOffButton;
  QPushButton *moveMoveBaseButton;

  void setupUi(QWidget *WaypointPanel) {
    if (WaypointPanel->objectName().isEmpty())
      WaypointPanel->setObjectName(QStringLiteral("WaypointPanel"));
    WaypointPanel->resize(356, 456);
    gridLayout_2 = new QGridLayout(WaypointPanel);
    gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
    gridLayout_2->setContentsMargins(0, 0, 0, 0);
    waypointPanelLayout = new QGridLayout();
    waypointPanelLayout->setObjectName(QStringLiteral("waypointPanelLayout"));
    waypointPanelLayout->setContentsMargins(9, -1, 9, -1);
    waypointTableWidget = new QTableWidget(WaypointPanel);
    waypointTableWidget->setObjectName(QStringLiteral("waypointTableWidget"));

    waypointPanelLayout->addWidget(waypointTableWidget, 0, 0, 1, 4);

    moveMoveBaseButton = new QPushButton(WaypointPanel);
    moveMoveBaseButton->setObjectName(QStringLiteral("moveMoveBaseButton"));

    waypointPanelLayout->addWidget(moveMoveBaseButton, 1, 0, 1, 4);

    gridLayout_2->addLayout(waypointPanelLayout, 0, 0, 1, 1);

    retranslateUi(WaypointPanel);

    QMetaObject::connectSlotsByName(WaypointPanel);
  }  // setupUi

  void retranslateUi(QWidget *WaypointPanel) {
    WaypointPanel->setWindowTitle(
        QApplication::translate("WaypointPanel", "Form", Q_NULLPTR));
    moveMoveBaseButton->setText(
        QApplication::translate("WaypointPanel", "Move(movebase)", Q_NULLPTR));
  }  // retranslateUi
};

namespace Ui {
class WaypointPanel : public Ui_WaypointPanel {};
}  // namespace Ui

QT_END_NAMESPACE

#endif  // UI_WAYPOINT_PANEL_H
