/********************************************************************************
** Form generated from reading UI file 'battery_panel.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BATTERY_PANEL_H
#define UI_BATTERY_PANEL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_BatteryPanel
{
public:
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout;
    QProgressBar *batteryProgressBar;

    void setupUi(QWidget *BatteryPanel)
    {
        if (BatteryPanel->objectName().isEmpty())
            BatteryPanel->setObjectName(QStringLiteral("BatteryPanel"));
        BatteryPanel->resize(400, 27);
        gridLayout_2 = new QGridLayout(BatteryPanel);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(2, 0, 2, 0);
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        batteryProgressBar = new QProgressBar(BatteryPanel);
        batteryProgressBar->setObjectName(QStringLiteral("batteryProgressBar"));
        batteryProgressBar->setValue(0);

        gridLayout->addWidget(batteryProgressBar, 0, 0, 1, 1);


        gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);


        retranslateUi(BatteryPanel);

        QMetaObject::connectSlotsByName(BatteryPanel);
    } // setupUi

    void retranslateUi(QWidget *BatteryPanel)
    {
        BatteryPanel->setWindowTitle(QApplication::translate("BatteryPanel", "BatteryPanel", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class BatteryPanel: public Ui_BatteryPanel {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BATTERY_PANEL_H
