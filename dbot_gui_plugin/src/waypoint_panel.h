/*

 */

#pragma once

#ifndef Q_MOC_RUN  // See: https://bugreports.qt-project.org/browse/QTBUG-22829

//ROS
#include <ros/ros.h>
#include <ros/package.h>
#include <geometry_msgs/Twist.h>
#include <dbot_msgs/DbotControlAction.h>
#include <actionlib/client/simple_action_client.h>

// Rviz
#include <rviz/panel.h>
#include <rviz/default_plugin/tools/move_tool.h>
#include <rviz/tool_manager.h>
#include <rviz/frame_manager.h>
#include <rviz/visualization_manager.h>

// QT
#include <QCursor>
#include <QObject>
#include <QPushButton>
#include <QComboBox>

// Boost
#include <boost/thread.hpp>

// this package
#include "key_tool.h"
#include "waypoint_tool.h"

#endif

namespace Ui {
	class WaypointPanel;
}

namespace dbot
{
class WaypointPanel : public rviz::Panel
{
  Q_OBJECT
public:
  explicit WaypointPanel(QWidget* parent = 0);
  virtual ~WaypointPanel();

	virtual void onInitialize();
  // virtual void load(const rviz::Config& config);
	// virtual void save(rviz::Config config) const;

private:
	Ui::WaypointPanel *ui;

	// Rviz tool maneger

public Q_SLOTS:
	void on_moveP2PButton_clicked();
	void on_moveMoveBaseButton_clicked();
	void UpdateWaypoint();

Q_SIGNALS:

protected:
	void StopThread(boost::thread& target_thread);
	void CheckP2PActionStatus();
	void CheckMoveBaseActionStatus();

	rviz::ToolManager* tool_manager_;

	WaypointTool* waypoint_tool_;

	std::vector<Ogre::SceneNode*>* waypoints_;

	actionlib::SimpleActionClient<dbot_msgs::DbotControlAction>* action_client_P2P_;
	actionlib::SimpleActionClient<dbot_msgs::DbotControlAction>* action_client_movebase_;

	boost::thread action_thread_P2P;
	boost::thread action_thread_movebase;
};
}  // namespace dbot
