/*

 */

#pragma once

#ifndef Q_MOC_RUN  // See: https://bugreports.qt-project.org/browse/QTBUG-22829

//ROS
#include <ros/ros.h>
#include <ros/package.h>
#include <geometry_msgs/Twist.h>

// Rviz
#include <rviz/tool.h>
#include <rviz/default_plugin/tools/move_tool.h>

// QT
#include <QCursor>
#include <QObject>
#endif

namespace dbot
{
class HumanFollowingTool : public rviz::Tool
{
  Q_OBJECT
public:
  HumanFollowingTool();
  virtual ~HumanFollowingTool();

  virtual void onInitialize();

  virtual void activate();
  virtual void deactivate();

  virtual int processMouseEvent(rviz::ViewportMouseEvent& event);

public Q_SLOTS:

protected:
	void Publish();

  ros::NodeHandle nh_;
	ros::Publisher human_foloowing_pub_;
};
}  // namespace dbot
