/********************************************************************************
** Form generated from reading UI file 'log_panel.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOG_PANEL_H
#define UI_LOG_PANEL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LogPanel
{
public:
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout;
    QGridLayout *gridLayout_3;
    QToolButton *clearButton;
    QSpacerItem *verticalSpacer;
    QTabWidget *tabWidget;
    QWidget *tab;
    QGridLayout *gridLayout_5;
    QGridLayout *gridLayout_4;
    QTextEdit *syslogTextEdit;
    QWidget *tab_2;
    QGridLayout *gridLayout_7;
    QGridLayout *gridLayout_6;
    QTextEdit *roslogTextEdit;

    void setupUi(QWidget *LogPanel)
    {
        if (LogPanel->objectName().isEmpty())
            LogPanel->setObjectName(QStringLiteral("LogPanel"));
        LogPanel->resize(400, 300);
        gridLayout_2 = new QGridLayout(LogPanel);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gridLayout_3->setContentsMargins(5, 5, 0, 5);
        clearButton = new QToolButton(LogPanel);
        clearButton->setObjectName(QStringLiteral("clearButton"));

        gridLayout_3->addWidget(clearButton, 0, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_3->addItem(verticalSpacer, 1, 0, 1, 1);


        gridLayout->addLayout(gridLayout_3, 0, 0, 1, 1);

        tabWidget = new QTabWidget(LogPanel);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        gridLayout_5 = new QGridLayout(tab);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        gridLayout_4 = new QGridLayout();
        gridLayout_4->setSpacing(6);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        syslogTextEdit = new QTextEdit(tab);
        syslogTextEdit->setObjectName(QStringLiteral("syslogTextEdit"));

        gridLayout_4->addWidget(syslogTextEdit, 0, 0, 1, 1);


        gridLayout_5->addLayout(gridLayout_4, 0, 0, 1, 1);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        gridLayout_7 = new QGridLayout(tab_2);
        gridLayout_7->setSpacing(6);
        gridLayout_7->setContentsMargins(11, 11, 11, 11);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        gridLayout_6 = new QGridLayout();
        gridLayout_6->setSpacing(6);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        roslogTextEdit = new QTextEdit(tab_2);
        roslogTextEdit->setObjectName(QStringLiteral("roslogTextEdit"));

        gridLayout_6->addWidget(roslogTextEdit, 0, 0, 1, 1);


        gridLayout_7->addLayout(gridLayout_6, 0, 0, 1, 1);

        tabWidget->addTab(tab_2, QString());

        gridLayout->addWidget(tabWidget, 0, 1, 1, 1);


        gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);


        retranslateUi(LogPanel);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(LogPanel);
    } // setupUi

    void retranslateUi(QWidget *LogPanel)
    {
        LogPanel->setWindowTitle(QApplication::translate("LogPanel", "LogPanel", Q_NULLPTR));
        clearButton->setText(QApplication::translate("LogPanel", "clear", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("LogPanel", "Sys Log", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("LogPanel", "ROS Log", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class LogPanel: public Ui_LogPanel {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOG_PANEL_H
