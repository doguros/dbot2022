#ifndef LOG_PANEL_H
#define LOG_PANEL_H

#include <QWidget>
#include <QString>

#include <rviz/panel.h>
#include <ros/ros.h>
#include <rosgraph_msgs/Log.h>

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <boost/thread.hpp>
using namespace std;

static int last_position=0; //read file untill nuew line

namespace Ui {
class LogPanel;
}
namespace dbot
{
class LogPanel : public rviz::Panel
{
    Q_OBJECT

public:
    explicit LogPanel(QWidget *parent = 0);
    ~LogPanel();

private:
    Ui::LogPanel *ui;

    ros::NodeHandle nh_;
    ros::Subscriber ros_log_sub_;
    
    boost::thread read_sys_log_thread_;
    //boost::thread read_ROS_log_thread_;

    int FindNewText(ifstream &infile);
    void ReadSyslogRun(string file_name);
    void ROSLogCallback(const rosgraph_msgs::Log::ConstPtr& log_msg);

private Q_SLOTS:
    void PrintSyslog(string log_text);
    void PrintROSlog(QString log_text);
    void ClearSyslogPanel();

Q_SIGNALS:
    void ReadSyslog(string log_text);
    void ReadROSlog(QString log_text);
};
}
#endif // LOG_PANEL_H
