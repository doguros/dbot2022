/*

 */

#pragma once

#ifndef Q_MOC_RUN  // See: https://bugreports.qt-project.org/browse/QTBUG-22829

//ROS
#include <ros/ros.h>
#include <ros/package.h>
#include <geometry_msgs/Twist.h>

// Rviz
#include <rviz/tool.h>
#include <rviz/default_plugin/tools/move_tool.h>

// QT
#include <QCursor>
#include <QObject>
#endif

namespace dbot
{
class KeyTool : public rviz::Tool
{
  Q_OBJECT
public:
  KeyTool();
  virtual ~KeyTool();

  virtual void onInitialize();

  virtual void activate();
  virtual void deactivate();

  virtual int processKeyEvent(QKeyEvent* event, rviz::RenderPanel* panel);
  virtual int processMouseEvent(rviz::ViewportMouseEvent& event);
	void SetCmdZero();

public Q_SLOTS:

protected:
	void PublishCmd();

  ros::NodeHandle nh_;
	ros::Publisher cmd_pub_;
	geometry_msgs::Twist cmd_;

	rviz::MoveTool move_tool_;
};
}  // namespace dbot
