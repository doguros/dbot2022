/********************************************************************************
** Form generated from reading UI file 'logging_set_dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGGING_SET_DIALOG_H
#define UI_LOGGING_SET_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>

QT_BEGIN_NAMESPACE

class Ui_ROSMessageLoggingDialog
{
public:
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout;
    QLabel *label;
    QLabel *pathCheckLabel;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_6;
    QGridLayout *gridLayout_5;
    QLineEdit *customTimeCycleValue;
    QRadioButton *timeCycleCustomRadio;
    QRadioButton *timeCycle10MinRadio;
    QRadioButton *timeCycleNotSetRadio;
    QRadioButton *timeCycle30MinRadio;
    QRadioButton *timeCycle1HourRadio;
    QRadioButton *timeCycle5MinRadio;
    QRadioButton *timeCycle15MinRadio;
    QRadioButton *timeCycle45MinRadio;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_4;
    QGridLayout *gridLayout_3;
    QListWidget *topicListWidget;
    QPushButton *savePathButton;
    QLineEdit *pathLineEdit;
    QPushButton *closePushButton;
    QPushButton *startPushButton;
    QPushButton *endPushButton;
    

    void setupUi(QDialog *ROSMessageLoggingDialog)
    {
        if (ROSMessageLoggingDialog->objectName().isEmpty())
            ROSMessageLoggingDialog->setObjectName(QStringLiteral("ROSMessageLoggingDialog"));
        ROSMessageLoggingDialog->resize(514, 422);
        gridLayout_2 = new QGridLayout(ROSMessageLoggingDialog);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label = new QLabel(ROSMessageLoggingDialog);
        label->setObjectName(QStringLiteral("label"));
        label->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label, 0, 0, 1, 1);

        pathCheckLabel = new QLabel(ROSMessageLoggingDialog);
        pathCheckLabel->setObjectName(QStringLiteral("pathCheckLabel"));

        gridLayout->addWidget(pathCheckLabel, 1, 0, 1, 3);

        groupBox_3 = new QGroupBox(ROSMessageLoggingDialog);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        gridLayout_6 = new QGridLayout(groupBox_3);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        gridLayout_5 = new QGridLayout();
        gridLayout_5->setSpacing(6);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        customTimeCycleValue = new QLineEdit(groupBox_3);
        customTimeCycleValue->setObjectName(QStringLiteral("customTimeCycleValue"));
        customTimeCycleValue->setMinimumSize(QSize(80, 0));
        customTimeCycleValue->setMaximumSize(QSize(80, 16777215));

        gridLayout_5->addWidget(customTimeCycleValue, 7, 1, 1, 1);

        timeCycleCustomRadio = new QRadioButton(groupBox_3);
        timeCycleCustomRadio->setObjectName(QStringLiteral("timeCycleCustomRadio"));

        gridLayout_5->addWidget(timeCycleCustomRadio, 7, 0, 1, 1);

        timeCycle10MinRadio = new QRadioButton(groupBox_3);
        timeCycle10MinRadio->setObjectName(QStringLiteral("timeCycle10MinRadio"));

        gridLayout_5->addWidget(timeCycle10MinRadio, 2, 0, 1, 2);

        timeCycleNotSetRadio = new QRadioButton(groupBox_3);
        timeCycleNotSetRadio->setObjectName(QStringLiteral("timeCycleNotSetRadio"));

        gridLayout_5->addWidget(timeCycleNotSetRadio, 0, 0, 1, 2);

        timeCycle30MinRadio = new QRadioButton(groupBox_3);
        timeCycle30MinRadio->setObjectName(QStringLiteral("timeCycle30MinRadio"));

        gridLayout_5->addWidget(timeCycle30MinRadio, 4, 0, 1, 2);

        timeCycle1HourRadio = new QRadioButton(groupBox_3);
        timeCycle1HourRadio->setObjectName(QStringLiteral("timeCycle1HourRadio"));

        gridLayout_5->addWidget(timeCycle1HourRadio, 6, 0, 1, 2);

        timeCycle5MinRadio = new QRadioButton(groupBox_3);
        timeCycle5MinRadio->setObjectName(QStringLiteral("timeCycle5MinRadio"));

        gridLayout_5->addWidget(timeCycle5MinRadio, 1, 0, 1, 2);

        timeCycle15MinRadio = new QRadioButton(groupBox_3);
        timeCycle15MinRadio->setObjectName(QStringLiteral("timeCycle15MinRadio"));

        gridLayout_5->addWidget(timeCycle15MinRadio, 3, 0, 1, 2);

        timeCycle45MinRadio = new QRadioButton(groupBox_3);
        timeCycle45MinRadio->setObjectName(QStringLiteral("timeCycle45MinRadio"));

        gridLayout_5->addWidget(timeCycle45MinRadio, 5, 0, 1, 2);


        gridLayout_6->addLayout(gridLayout_5, 0, 0, 1, 1);


        gridLayout->addWidget(groupBox_3, 2, 4, 1, 2);

        groupBox = new QGroupBox(ROSMessageLoggingDialog);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        gridLayout_4 = new QGridLayout(groupBox);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        topicListWidget = new QListWidget(groupBox);
        topicListWidget->setObjectName(QStringLiteral("topicListWidget"));
        topicListWidget->setFlow(QListView::TopToBottom);
        topicListWidget->setResizeMode(QListView::Adjust);

        gridLayout_3->addWidget(topicListWidget, 0, 0, 1, 1);


        gridLayout_4->addLayout(gridLayout_3, 0, 0, 1, 2);


        gridLayout->addWidget(groupBox, 2, 0, 1, 4);

        savePathButton = new QPushButton(ROSMessageLoggingDialog);
        savePathButton->setObjectName(QStringLiteral("savePathButton"));

        gridLayout->addWidget(savePathButton, 0, 5, 1, 1);

        pathLineEdit = new QLineEdit(ROSMessageLoggingDialog);
        pathLineEdit->setObjectName(QStringLiteral("pathLineEdit"));

        gridLayout->addWidget(pathLineEdit, 0, 1, 1, 4);

        closePushButton = new QPushButton(ROSMessageLoggingDialog);
        closePushButton->setObjectName(QStringLiteral("closePushButton"));

        gridLayout->addWidget(closePushButton, 3, 5, 1, 1);

        startPushButton = new QPushButton(ROSMessageLoggingDialog);
        startPushButton->setObjectName(QStringLiteral("startPushButton"));

        gridLayout->addWidget(startPushButton, 3, 0, 1, 2);

        endPushButton = new QPushButton(ROSMessageLoggingDialog);
        endPushButton->setObjectName(QStringLiteral("endPushButton"));

        gridLayout->addWidget(endPushButton, 3, 2, 1, 2);

        gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);


        retranslateUi(ROSMessageLoggingDialog);

        QMetaObject::connectSlotsByName(ROSMessageLoggingDialog);
    } // setupUi

    void retranslateUi(QDialog *ROSMessageLoggingDialog)
    {
        ROSMessageLoggingDialog->setWindowTitle(QApplication::translate("ROSMessageLoggingDialog", "ROSMessageLoggingDialog", nullptr));
        label->setText(QApplication::translate("ROSMessageLoggingDialog", "Save Path: ", nullptr));
        pathCheckLabel->setText(QString());
        groupBox_3->setTitle(QApplication::translate("ROSMessageLoggingDialog", "Time Cycle Setting", nullptr));
        timeCycleCustomRadio->setText(QApplication::translate("ROSMessageLoggingDialog", "Time(Min)", nullptr));
        timeCycle10MinRadio->setText(QApplication::translate("ROSMessageLoggingDialog", "10 Min", nullptr));
        timeCycleNotSetRadio->setText(QApplication::translate("ROSMessageLoggingDialog", "Not set", nullptr));
        timeCycle30MinRadio->setText(QApplication::translate("ROSMessageLoggingDialog", "30 Min", nullptr));
        timeCycle1HourRadio->setText(QApplication::translate("ROSMessageLoggingDialog", "1 Hour", nullptr));
        timeCycle5MinRadio->setText(QApplication::translate("ROSMessageLoggingDialog", "5 Min", nullptr));
        timeCycle15MinRadio->setText(QApplication::translate("ROSMessageLoggingDialog", "15 Min", nullptr));
        timeCycle45MinRadio->setText(QApplication::translate("ROSMessageLoggingDialog", "45 Min", nullptr));
        groupBox->setTitle(QApplication::translate("ROSMessageLoggingDialog", "Topic List", nullptr));
        savePathButton->setText(QApplication::translate("ROSMessageLoggingDialog", "...", nullptr));
        closePushButton->setText(QApplication::translate("ROSMessageLoggingDialog", "Close", nullptr));
        startPushButton->setText(QApplication::translate("ROSMessageLoggingDialog", "Logging Start", nullptr));
        endPushButton->setText(QApplication::translate("ROSMessageLoggingDialog", "Logging End", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ROSMessageLoggingDialog: public Ui_ROSMessageLoggingDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGGING_SET_DIALOG_H
