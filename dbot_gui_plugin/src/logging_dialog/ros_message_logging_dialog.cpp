#include "ros_message_logging_dialog.h"


ROSMessageLoggingDialog::ROSMessageLoggingDialog(QWidget *parent)
	: time_cycle_(-1), time_cycle_radio_index_(0), time_cycle_thread_flag_(false), time_cycle_thread_run_flag_(false), QDialog(parent)
{
	ui.setupUi(this);

	Initialize();
}

ROSMessageLoggingDialog::~ROSMessageLoggingDialog()
{

}

void ROSMessageLoggingDialog::Initialize() {
	logging_process_ = new QProcess(this);
	GetTopicList();

	ui.endPushButton->setEnabled(false);
	ui.timeCycleNotSetRadio->setChecked(true);

	connect(ui.timeCycleNotSetRadio,	SIGNAL(clicked(bool)), this, SLOT(SetTimeCycle()));
	connect(ui.timeCycle5MinRadio,		SIGNAL(clicked(bool)), this, SLOT(SetTimeCycle()));
	connect(ui.timeCycle10MinRadio,		SIGNAL(clicked(bool)), this, SLOT(SetTimeCycle()));
	connect(ui.timeCycle15MinRadio,		SIGNAL(clicked(bool)), this, SLOT(SetTimeCycle()));
	connect(ui.timeCycle30MinRadio,		SIGNAL(clicked(bool)), this, SLOT(SetTimeCycle()));
	connect(ui.timeCycle45MinRadio,		SIGNAL(clicked(bool)), this, SLOT(SetTimeCycle()));
	connect(ui.timeCycle1HourRadio,		SIGNAL(clicked(bool)), this, SLOT(SetTimeCycle()));
	connect(ui.timeCycleCustomRadio,	SIGNAL(clicked(bool)), this, SLOT(SetTimeCycle()));

	connect(ui.pathLineEdit, SIGNAL(textChanged(QString)), this, SLOT(CheckBagFileSavePath()));
	connect(ui.closePushButton, SIGNAL(clicked()), this, SLOT(close()));

	ui.pathCheckLabel->setStyleSheet("color : red;");
}

void ROSMessageLoggingDialog::showEvent(QShowEvent *e){
	QDialog::showEvent(e);
	GetTopicList();
}

void ROSMessageLoggingDialog::on_startPushButton_clicked() {
	
	if (ui.pathLineEdit->text().isEmpty()) {
		QMessageBox message_box;
		message_box.setText("Path is empty.");
		message_box.exec();
		return;
	}

	if (!ui.pathCheckLabel->text().isEmpty()) {
		QMessageBox message_box;
		message_box.setText("The given path is not a valid directory or does not exist.");
		message_box.exec();
		return;
	}

	switch (time_cycle_radio_index_) {
		case 0:
			time_cycle_ = -1;
			break;
		case 1:
			time_cycle_ = 5;
			break;
		case 2:
			time_cycle_ = 10;
			break;
		case 3:
			time_cycle_ = 15;
			break;
		case 4:
			time_cycle_ = 30;
			break;
		case 5:
			time_cycle_ = 45;
			break;
		case 6:
			time_cycle_ = 60;
			break;
		case 7:
			if (ui.customTimeCycleValue->text().isEmpty()) {
				QMessageBox error_message_box;
				error_message_box.setText("Please input time cycle value.");
				error_message_box.exec();
				return;
			}

			time_cycle_ = ui.customTimeCycleValue->text().toInt();
			break;
	}
	ROS_INFO("time_cycle value: %d",time_cycle_);
	if (time_cycle_ == -1){
		time_cycle_thread_flag_ = false;
		boost::thread Logging_thread(&ROSMessageLoggingDialog::LoggingRun, this);
	} else {
		time_cycle_thread_flag_ = true;
		time_cycle_thread_run_flag_ = true;
		boost::thread Logging_thread(&ROSMessageLoggingDialog::TimeCycleLogging, this);
	
	}
	LoggingStart();

	ui.startPushButton->setEnabled(false);
	ui.endPushButton->setEnabled(true);
}
void ROSMessageLoggingDialog::on_endPushButton_clicked(){

	LoggingEnd();

	ui.startPushButton->setEnabled(true);
	ui.endPushButton->setEnabled(false);
}

void ROSMessageLoggingDialog::GetTopicList(){
	qDebug() << "GetTopicList in ROSMessageLoggingDialog";
	//ListWidget reset
	ui.topicListWidget->clear();

	//Get Topic List
	ros::master::V_TopicInfo topic_infos;
  	ros::master::getTopics(topic_infos);

	for(auto topic_info : topic_infos){
		qDebug() << "topic: " << topic_info.name.c_str();
		QListWidgetItem* item = new QListWidgetItem;
		item->setData(Qt::DisplayRole, topic_info.name.c_str());
		item->setData(Qt::CheckStateRole, false);
		ui.topicListWidget->addItem(item);
	}
}

void ROSMessageLoggingDialog::LoggingRun(){

	QStringList topic_list;
	topic_list.append("record");
	topic_list.append("-o");
	topic_list.append(ui.pathLineEdit->text()+"/");
	
	for(int tl_i = 0 ; tl_i < ui.topicListWidget->count() ; tl_i++){
		QListWidgetItem* item = ui.topicListWidget->item(tl_i);
		if(item->checkState())
			topic_list.append(item->text());
	}
	topic_list.append("__name:=ros_message_logging_bag");
	
	qDebug() << topic_list;
	/* rosbag execute */
    logging_process_->execute("rosbag", topic_list);
	
	qDebug() << "finish";
}

void ROSMessageLoggingDialog::LoggingEnd(){
	if(!time_cycle_thread_flag_)
		boost::thread logging_end_thread(&ROSMessageLoggingDialog::LoggingEndRun, this);
	else{
		time_cycle_thread_run_flag_ = false;
		boost::thread logging_end_thread(&ROSMessageLoggingDialog::CycleLoggingEndRun, this, thread_index_);
	}
}
void ROSMessageLoggingDialog::LoggingEndRun(){
	QProcess::execute("rosnode",QStringList() << "kill" << "/ros_message_logging_bag");
}

void ROSMessageLoggingDialog::CycleLoggingRun(int thread_index){

	QStringList topic_list;
	topic_list.append("record");
	topic_list.append("-o");
	topic_list.append(ui.pathLineEdit->text()+"/");
	
	for(int tl_i = 0 ; tl_i < ui.topicListWidget->count() ; tl_i++){
		QListWidgetItem* item = ui.topicListWidget->item(tl_i);
		if(item->checkState())
			topic_list.append(item->text());
	}
	topic_list.append(QString("__name:=ros_message_logging_bag_%1").arg(QString::number(thread_index)));
	
	qDebug() << topic_list;
	/* rosbag execute */
    logging_process_->execute("rosbag", topic_list);
	
	qDebug() << "finish";
}
void ROSMessageLoggingDialog::CycleLoggingEndRun(int thread_index){

	QProcess::execute("rosnode",QStringList() << "kill" << QString("/ros_message_logging_bag_%1").arg(QString::number(thread_index)));
}

void ROSMessageLoggingDialog::TimeCycleLogging(){
	thread_index_ = 0;
	while(1){	
		boost::thread Logging_thread(&ROSMessageLoggingDialog::CycleLoggingRun, this, thread_index_);
		sleep(time_cycle_ * 60);

		if(!time_cycle_thread_run_flag_)
			break;

		boost::thread logging_end_thread(&ROSMessageLoggingDialog::CycleLoggingEndRun, this, thread_index_++);
	}
}

void ROSMessageLoggingDialog::on_savePathButton_clicked() {
	QString path = QFileDialog::getExistingDirectory(0, ("Select Folder"), QDir::currentPath());
	
	if (path.isEmpty())
		return;

	ui.pathLineEdit->setText(path);
}

void ROSMessageLoggingDialog::CheckBagFileSavePath() {

	const QFileInfo Dir(ui.pathLineEdit->text());

	if((!Dir.exists())){
		if(ui.pathLineEdit->text().isEmpty())
			ui.pathCheckLabel->setText("");
		else
			ui.pathCheckLabel->setText("The given path is not a valid directory or does not exist.");
	}
	else
		ui.pathCheckLabel->setText("");
}

void ROSMessageLoggingDialog::SetTimeCycle() {
	QRadioButton* time_cycle_radio = (QRadioButton*)sender();

	if (time_cycle_radio == ui.timeCycleNotSetRadio)
		time_cycle_radio_index_ = 0;
	else if (time_cycle_radio == ui.timeCycle5MinRadio)
		time_cycle_radio_index_ = 1;
	else if (time_cycle_radio == ui.timeCycle10MinRadio)
		time_cycle_radio_index_ = 2;
	else if (time_cycle_radio == ui.timeCycle15MinRadio)
		time_cycle_radio_index_ = 3;
	else if (time_cycle_radio == ui.timeCycle30MinRadio)
		time_cycle_radio_index_ = 4;
	else if (time_cycle_radio == ui.timeCycle45MinRadio)
		time_cycle_radio_index_ = 5;
	else if (time_cycle_radio == ui.timeCycle1HourRadio)
		time_cycle_radio_index_ = 6;
	else if (time_cycle_radio == ui.timeCycleCustomRadio)
		time_cycle_radio_index_ = 7;
}