#pragma once

#include <QDialog>
#include <QListWidgetItem>
#include <QDebug>
#include <QProcess>
#include <QShowEvent>
#include <QFileDialog>
#include <QMessageBox>

#include <ros/master.h>
#include <boost/thread.hpp>

#include "ui_ros_message_logging_dialog.h"

class ROSMessageLoggingDialog : public QDialog
{
	Q_OBJECT

public:
	ROSMessageLoggingDialog(QWidget *parent = Q_NULLPTR);
	~ROSMessageLoggingDialog();

protected:
	void 					showEvent(QShowEvent *e);

private:
	Ui::ROSMessageLoggingDialog 	ui;
	QProcess 				*logging_process_;

	int time_cycle_radio_index_;
	int time_cycle_;	// 단위: 분

	int thread_index_;
	bool time_cycle_thread_run_flag_;
	bool time_cycle_thread_flag_;	


private Q_SLOTS:
	void 					Initialize();

	void 					on_startPushButton_clicked();
	void					on_endPushButton_clicked();
	void 					on_savePathButton_clicked();
	void 					CheckBagFileSavePath();

	void 					LoggingRun();
	void 					LoggingEnd();
	void					LoggingEndRun();

	void 					CycleLoggingRun(int thread_index);
	void					CycleLoggingEndRun(int thread_index);
	void 					TimeCycleLogging();

	void 					SetTimeCycle();
	void 					GetTopicList();
	
Q_SIGNALS:
	void 					StartLogging(QString path);
	void 					LoggingStart();
	
};
