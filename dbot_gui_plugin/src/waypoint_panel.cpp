

#include "waypoint_panel.h"
#include "ui_waypoint_panel.h"

#include <QString>
namespace dbot {

WaypointPanel::WaypointPanel(QWidget* parent)
    : rviz::Panel(parent), ui(new Ui::WaypointPanel) {
  ui->setupUi(this);

  ui->moveMoveBaseButton->setCheckable(true);
  ui->waypointTableWidget->setColumnCount(2);
  ui->waypointTableWidget->setShowGrid(true);
  ui->waypointTableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
  ui->waypointTableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
  ui->waypointTableWidget->verticalHeader()->setVisible(true);
  ui->waypointTableWidget->setHorizontalHeaderLabels(
      QStringList() << trUtf8("X") << trUtf8("Y"));
  ui->waypointTableWidget->horizontalHeader()->setSectionResizeMode(
      QHeaderView::Stretch);
}

WaypointPanel::~WaypointPanel() {
  delete ui;
  StopThread(action_thread_movebase);
}

void WaypointPanel::onInitialize() {
  // Waypoint Tool initialzing
  QStringList tool_list = vis_manager_->getToolManager()->getToolClasses();
  int tool_idx = -1;
  for (int i = 0; i < tool_list.size(); i++) {
    if (tool_list[i] == "dbot_gui_plugin/WaypointTool") {
      tool_idx = i;
    }
  }

  rviz::Tool* waypoint_tool = vis_manager_->getToolManager()->getTool(tool_idx);
  waypoint_tool_ = static_cast<WaypointTool*>(waypoint_tool);
  vis_manager_->getToolManager()->setDefaultTool(waypoint_tool_);
  vis_manager_->getToolManager()->setCurrentTool(waypoint_tool_);

  QObject::connect(waypoint_tool_, SIGNAL(WaypointUpdated()), this,
                   SLOT(UpdateWaypoint()));

  // Waypoint initialzing
  waypoints_ = waypoint_tool_->GetWaypoints();

  // dbot_movebase Action client initializing
  action_client_movebase_ =
      new actionlib::SimpleActionClient<dbot_msgs::DbotControlAction>(
          "dbot_movebase_action", true);
}

void WaypointPanel::UpdateWaypoint() {
  ui->waypointTableWidget->setRowCount(0);
  for (size_t n_c = 0; n_c < waypoints_->size(); n_c++) {
    ui->waypointTableWidget->insertRow(n_c);
    ui->waypointTableWidget->setItem(
        n_c, 0,
        new QTableWidgetItem(
            QString::number(waypoints_->at(n_c)->getPosition().x)));
    ui->waypointTableWidget->item(n_c, 0)->setTextAlignment(Qt::AlignCenter);
    ui->waypointTableWidget->item(n_c, 0)->setFlags(
        ui->waypointTableWidget->item(n_c, 0)->flags() & ~Qt::ItemIsEditable);

    ui->waypointTableWidget->setItem(
        n_c, 1,
        new QTableWidgetItem(
            QString::number(waypoints_->at(n_c)->getPosition().y)));
    ui->waypointTableWidget->item(n_c, 1)->setTextAlignment(Qt::AlignCenter);
    ui->waypointTableWidget->item(n_c, 1)->setFlags(
        ui->waypointTableWidget->item(n_c, 1)->flags() & ~Qt::ItemIsEditable);
  }
}

void WaypointPanel::StopThread(boost::thread& target_thread) {
  if (target_thread.joinable()) {
    target_thread.join();
  }
}

void WaypointPanel::CheckMoveBaseActionStatus() {
  action_client_movebase_->waitForResult();
  ui->moveMoveBaseButton->setChecked(false);
}

void WaypointPanel::on_moveMoveBaseButton_clicked() {
  if (action_client_movebase_->getState() ==
      actionlib::SimpleClientGoalState::ACTIVE) {
    // If the robot is moving for movebase action
    ROS_WARN("Action MoveBase is canceled");
    action_client_movebase_->cancelAllGoals();
    ui->moveMoveBaseButton->setChecked(false);
    StopThread(action_thread_movebase);
    return;
  } else {
    // If the robot is moving for a action.
    if (action_client_movebase_->getState() ==
        actionlib::SimpleClientGoalState::ACTIVE) {
      ROS_WARN("Other action is active. Please wait the action or cancel it");
      ui->moveMoveBaseButton->setChecked(false);
      return;
    }
  }

  dbot_msgs::DbotControlGoal goal;
  dbot_msgs::DrivingMode driving_mode;
  goal.driving_mode.driving_mode = 1;

  for (size_t i = 0; i < waypoints_->size(); i++) {
    geometry_msgs::Pose waypoint;
    waypoint.position.x = waypoints_->at(i)->getPosition().x;
    waypoint.position.y = waypoints_->at(i)->getPosition().y;
    waypoint.position.z = waypoints_->at(i)->getPosition().z;
    waypoint.orientation.x = 0; 
    waypoint.orientation.y = 0;
    waypoint.orientation.z = 1;
    waypoint.orientation.w = 0;
    goal.waypoints.push_back(waypoint);
  }

  action_client_movebase_->sendGoal(goal);

  action_thread_movebase =
      boost::thread(&WaypointPanel::CheckMoveBaseActionStatus, this);
}

}  // namespace dbot

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(dbot::WaypointPanel, rviz::Panel)
