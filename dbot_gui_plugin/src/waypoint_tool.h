#ifndef DBOT_GUI_PLUGIN_WAYPOINT_TOOL_H
#define DBOT_GUI_PLUGIN_WAYPOINT_TOOL_H

//Ogre
#include <OgreVector3.h>

//ROS
#include <ros/ros.h>
#include <geometry_msgs/Pose.h>

//Rviz
#include <rviz/default_plugin/tools/move_tool.h>
#include <rviz/tool.h>
#include <rviz/ogre_helpers/line.h>

//QT
#include <QCursor>

#include <boost/thread.hpp>

namespace rviz {
	class Arrow;
	class DisplayContext;
	class VectorProperty;
} // namespace rviz

namespace dbot {

class WaypointTool: public rviz::Tool
{
	Q_OBJECT
	
public:
	WaypointTool();
	virtual ~WaypointTool();

	virtual void onInitialize();
	virtual void activate();
	virtual void deactivate();

	virtual int processMouseEvent(rviz::ViewportMouseEvent& event);

	std::vector<Ogre::SceneNode*>* GetWaypoints();
	void RemoveAllWaypoint();
	void RemoveLastWaypoint();
	void RemoveWaypoint(int idx);
	void MakeWaypoint(const Ogre::Vector3& point);
	void MakeCurrentPoseToWaypoint();
	void RemoveLine(int idx);
	void AddLine(int idx_src, int idx_dst);
	void AttachWaypointShape(Ogre::SceneNode* node);

protected:
	void RobotPoseUpdate(const geometry_msgs::Pose::ConstPtr& msg);

	rviz::Arrow* arrow_;

	enum State {
		Activate,
		Deactivate
	};

	Ogre::Vector3 pos_;

	Ogre::SceneNode* moving_waypoint_node_;
	std::vector<Ogre::SceneNode*>* waypoint_nodes_;
	std::vector<rviz::Line*> line_nodes_;

	rviz::VectorProperty* current_waypoint_property_;
	rviz::VectorProperty* clicked_waypoint_property_;

	rviz::MoveTool move_tool_;

private:

	void StartThread();
	void StopThread();

	void SubscribeRobotPose();

	boost::thread pose_subcribe_thread_;	

	geometry_msgs::Pose robot_pose_;

	State state_;

	ros::NodeHandle nh_;
  ros::Subscriber sub_pose_;

public Q_SLOTS:
	void UpdateWaypoint(int idx, double x, double y);

Q_SIGNALS:
	void WaypointUpdated();
}; // class WaypointTool

}// namespace dbot

#endif // DBOT_GUI_PLUGIN_WAYPOINT_TOOL_H