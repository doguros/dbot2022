#include "battery_panel.h"
#include "ui_battery_panel.h"

namespace dbot{

BatteryPanel::BatteryPanel(QWidget *parent) :
    rviz::Panel(parent),
    ui(new Ui::BatteryPanel)
{
    ui->setupUi(this);

    battery_sub_ = nh_.subscribe("/robot_status",1000, &BatteryPanel::BatteryCallback, this);

    connect(this, SIGNAL(ReadBattery(int)),this,SLOT(SetBattery(int)));
}

BatteryPanel::~BatteryPanel()
{
    delete ui;
}

void BatteryPanel::BatteryCallback(const dbot_msgs::RobotStatus::ConstPtr& msg){
    ReadBattery(msg->battery);
}

void BatteryPanel::SetBattery(int battery){
    ui->batteryProgressBar->setValue(battery);
}

}
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(dbot::BatteryPanel, rviz::Panel)
