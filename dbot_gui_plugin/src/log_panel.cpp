#include "log_panel.h"
#include "ui_log_panel.h"

namespace dbot
{
LogPanel::LogPanel(QWidget *parent) :
    rviz::Panel(parent),
    ui(new Ui::LogPanel)
{
    ui->setupUi(this);
    qRegisterMetaType<string>("string");


    ros_log_sub_ = nh_.subscribe("rosout",1000, &LogPanel::ROSLogCallback, this);

    connect(this, SIGNAL(ReadSyslog(string)),this,SLOT(PrintSyslog(string)));
    connect(this, SIGNAL(ReadROSlog(QString)),this,SLOT(PrintROSlog(QString)));
    connect(ui->clearButton,SIGNAL(clicked()),this,SLOT(ClearSyslogPanel()));
    
    read_sys_log_thread_ = boost::thread(boost::bind(&LogPanel::ReadSyslogRun,this,"/var/log/syslog"));
    //read_ROS_log_thread_ = boost::thread(boost::bind(&LogPanel::ROSLogCallback,this));

}

LogPanel::~LogPanel()
{
    delete ui;
}

int LogPanel::FindNewText(ifstream &infile){
    infile.seekg(0,ios::end);
    int filesize = infile.tellg();

    //check if the new file started
    if(filesize < last_position){
        last_position=0;
    }

    // read file from last position untill new line is found
    for(int n =last_position ; n<filesize ; n++){
        infile.seekg(last_position,ios::beg);
        //char text[256];
        //infile.getline(text, 256);
        string log_text;
        getline(infile,log_text);
        last_position = infile.tellg();

        if(!log_text.empty())
            ReadSyslog(log_text);

        // end of file
        if(filesize == last_position){
            return filesize;
        }
    }
    return 0;
}

void LogPanel::ReadSyslogRun(string file_name){
    while(1){
        std::ifstream infile(file_name);
        FindNewText(infile);
        sleep(1);
    }
}

void LogPanel::PrintSyslog(string log_text){
    ui->syslogTextEdit->append(log_text.c_str());
}

void LogPanel::ClearSyslogPanel(){
    ui->syslogTextEdit->setText("");
    ui->roslogTextEdit->setText("");
}

void LogPanel::ROSLogCallback(const rosgraph_msgs::Log::ConstPtr& log_msg){
    
    //while(1){
        
        //boost::shared_ptr<rosgraph_msgs::Log const> log_msg =
        //    ros::topic::waitForMessage<rosgraph_msgs::Log>("rosout",nh_,ros::Duration(1));
        
        //if(!log_msg){
        //    continue;
        //}

        string msg_info;
        switch(log_msg->level){
            case rosgraph_msgs::Log::DEBUG:
                msg_info = "[DEBUG]";
                break;
            case rosgraph_msgs::Log::INFO:
                msg_info = "[INFO]";
                break;
            case rosgraph_msgs::Log::WARN:
                msg_info = "[WARN]";
                break;
            case rosgraph_msgs::Log::ERROR:
                msg_info = "[ERROR]";
                break;
            case rosgraph_msgs::Log::FATAL:
                msg_info = "[FATAL]";
                break;
        }   
        string topic_info; 
        for(int topic_index = 0 ; topic_index < log_msg->topics.size() ; topic_index++){
            string  topic = log_msg->topics.at(topic_index);
            if(topic.compare("rosout"))
            topic_info.append(topic);
            topic_info.append(", ");
        }
        if(!topic_info.empty()){
            topic_info.pop_back();
            topic_info.pop_back();
        }
        QString log = QString("[%1.%2] [%3] [%4::%5(%6)] [topic: %7] %8")
                            .arg(QString::number(log_msg->header.stamp.sec))
                            .arg(QString::number(log_msg->header.stamp.nsec))
                            .arg(QString::fromStdString(msg_info))
                            .arg(QString::fromStdString(log_msg->file))
                            .arg(QString::number(log_msg->line))
                            .arg(QString::fromStdString(log_msg->function))
                            .arg(QString::fromStdString(topic_info))
                            .arg(QString::fromStdString(log_msg->msg));

        
        ReadROSlog(log);
   // }
}

void LogPanel::PrintROSlog(QString log_text){
    ui->roslogTextEdit->append(log_text);
}

}
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(dbot::LogPanel, rviz::Panel)
