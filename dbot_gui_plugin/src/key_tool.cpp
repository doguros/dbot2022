/*
 * Copyright (c) 2013, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <OgreRay.h>
#include <OgreVector3.h>

// Rviz
#include <rviz/display_context.h>
#include <rviz/load_resource.h>
#include <rviz/properties/bool_property.h>
#include <rviz/properties/string_property.h>
#include <rviz/view_controller.h>
#include <rviz/viewport_mouse_event.h>

// this package
#include "key_tool.h"

// C++
#include <sstream>

namespace dbot
{
KeyTool::KeyTool() = default;

KeyTool::~KeyTool() {
	ROS_INFO("KeyTool Deleted");
}

void KeyTool::onInitialize()
{
	setIcon(rviz::loadPixmap("package://dbot_gui_plugin/icons/classes/KeyTool.svg"));
  move_tool_.initialize(context_);
}

void KeyTool::activate()
{
	cmd_pub_ = nh_.advertise<geometry_msgs::Twist>("/cmd_vel", 1);
}

void KeyTool::deactivate()
{
}

int KeyTool::processKeyEvent(QKeyEvent* event, rviz::RenderPanel* panel)
{
  // move forward / backward
  switch (event->key())
  {
    case Qt::Key_W:
			cmd_.linear.x = cmd_.linear.x + 0.05;
			
      break;
    case Qt::Key_D:
			cmd_.angular.z = cmd_.angular.z - 0.05;
			
			break;
    case Qt::Key_A:
      cmd_.angular.z = cmd_.angular.z + 0.05;
			
			break;
    case Qt::Key_X:
      cmd_.linear.x = cmd_.linear.x - 0.05;
      
			break;
    case Qt::Key_S:
			SetCmdZero();
			break;
		default:
			return move_tool_.processKeyEvent(event, panel);
  }
	PublishCmd();
}

int KeyTool::processMouseEvent(rviz::ViewportMouseEvent& event)
{
  int flags = 0;
	
  move_tool_.processMouseEvent(event);
  setCursor(move_tool_.getCursor());

  return flags;
}

void KeyTool::SetCmdZero() {
	cmd_.angular.x = 0;
	cmd_.angular.y = 0;
	cmd_.angular.z = 0;
	cmd_.linear.x = 0;
	cmd_.linear.y = 0;
	cmd_.linear.z = 0;
}

void KeyTool::PublishCmd() {
	if (cmd_.linear.x > 1.0) {
		cmd_.linear.x = 1.0;
	}
	if (cmd_.linear.x < -1.0) {
		cmd_.linear.x = -1.0;
	}
	if (cmd_.linear.x < 0.001 && cmd_.linear.x > -0.001) {
		cmd_.linear.x = 0.0;
	}

	if (cmd_.angular.z < -1.0) {
		cmd_.angular.z = -1.0;
	}
	if (cmd_.angular.z > 1.0) {
		cmd_.angular.z = 1.0;
	}
	if (cmd_.angular.z < 0.001 && cmd_.angular.z > -0.001) {
		cmd_.angular.z = 0.0;
	}

	cmd_pub_.publish(cmd_);
}

}  // namespace dbot

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(dbot::KeyTool, rviz::Tool)
