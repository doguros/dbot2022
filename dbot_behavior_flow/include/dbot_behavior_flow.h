#include <actionlib/client/simple_action_client.h>
#include <dbot_msgs/DbotControlAction.h>
#include <visualization_msgs/MarkerArray.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>
#include <ros/package.h>
#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <yaml-cpp/yaml.h>
#include <std_srvs/Empty.h>

#include <boost/thread.hpp>
#include <fstream>

#include "dbot_msgs/RobotState.h"
#include "dbot_msgs/RobotStatus.h"
#include "dbot_msgs/RobotSwitch.h"
#include "tf/transform_listener.h"

template <typename T>
void operator>>(const YAML::Node& node, T& i) {
  i = node.as<T>();
}

// parameter struct and typedef
struct BehaviorFlowParam {
  // waypoint filename
  std::string path_filename;
  std::string home_path_filename;
};

enum DbotState {
  IDLE,
  PATROL,
  ATOB,
  ATOH,
  HOLD,
};

ros::Subscriber joy_sub;
sensor_msgs::Joy joy_msg;
geometry_msgs::Twist hold_vel_;
geometry_msgs::Twist cmd_vel_;
ros::Subscriber robot_state_sub;
ros::Subscriber current_pose_sub_;
ros::Subscriber robot_status_sub;
ros::Subscriber robot_switch_sub;
ros::Subscriber constraint_sub;
ros::Subscriber clicked_point_sub;
ros::Publisher robot_state_pub;
ros::Publisher robot_status_pub;
ros::Publisher robot_switch_pub;
ros::Publisher hold_vel_pub;
ros::Publisher cmd_vel_pub;
dbot_msgs::RobotStatus robot_status_msg;
dbot_msgs::RobotStatus robot_status_;
dbot_msgs::RobotSwitch robot_switch_;
dbot_msgs::RobotState robot_state_msg;
dbot_msgs::RobotState robot_state_;
dbot_msgs::RobotState robot_state_saved_;
dbot_msgs::RobotState robot_state_old_;

ros::ServiceClient clear_costmap_service_;
std_srvs::Empty srv;

ros::Time current_time, last_time, clear_current_time, clear_last_time;

int activate_num = 5;              // RB
int idle_num = 9;                  // START
int atob_num = 1;                  // A
int atoh_num = 0;                  // X
int patrol_num = 3;                // Y
int hold_num = 2;                  // B
int manual_num = 0;                // X
int activate_waypoint_num = 7;     // RT
int set_waypoint_num = 1;          // A
int remove_last_waypoint_num = 0;  // X
int clear_all_waypoints_num = 3;   // Y
int cancel_waypoints_num = 2;      // B
int bumper_deactivate_num = 8;     // back
int bumper_activate_num = 9;       // start
int front_light_num = 1;
int water_pump_num = 2;
int uvc_lamp_num = 0;
int uva_lamp_num = 11;
int linear_motor_num = 3;
int call_num = 3;

bool joy_flag = false;

ros::Duration duration_min(1);
ros::Duration clear_duration_min(1);

///
BehaviorFlowParam params_;
/// input image file name including the path
std::string path_filename_;
std::string home_path_filename_;

geometry_msgs::Pose current_pose_;

std::vector<geometry_msgs::Pose*>* waypoints_;
std::vector<geometry_msgs::Pose*>* path_;

actionlib::SimpleActionClient<dbot_msgs::DbotControlAction>* action_client_P2P_;
actionlib::SimpleActionClient<dbot_msgs::DbotControlAction>*
    action_client_movebase_;

boost::thread action_thread_P2P;
boost::thread action_thread_movebase;

dbot_msgs::DbotControlGoal goal;

void StopThread(boost::thread& target_thread);
void CheckP2PActionStatus();
void CheckMoveBaseActionStatus();

void RemoveAllWaypoint();
void RemoveLastWaypoint();
void MakeCurrentPoseToWaypoint();
void FollowWaypoints();
void FollowPatrolPath();
void ResumeFollowWaypoints();
void ResumeFollowPatrolPath();
bool GetPathFromFile();
void CancelFollowWaypoints();
void HoldEveryFunction();
void SendHome();
void ResumeState();
double DistanceCalculatorFromCurrPose(double x, double y);