#include "dbot_behavior_flow.h"

void SubRobotState(const dbot_msgs::RobotState& msg) {
  robot_state_msg.robot_state = msg.robot_state;
  robot_state_.robot_state = msg.robot_state;
}

void SubRobotStatus(const dbot_msgs::RobotStatus& msg) {
  robot_status_msg = msg;
}

void SubRobotSwitch(const dbot_msgs::RobotSwitch& msg) {
  robot_switch_ = msg;
}

void SubConstraintList(const visualization_msgs::MarkerArray::ConstPtr& msg)
{
  robot_state_.local_state = msg->markers[4].points.size();
  robot_state_pub.publish(robot_state_);
}

void SubClickedPoint(const geometry_msgs::PointStamped::ConstPtr& msg) {
  geometry_msgs::Pose* saved_point = new geometry_msgs::Pose();
  double roll, pitch, yaw;
  tf::Quaternion q;
  if (waypoints_->size() != 0) {
    yaw =
        atan2(msg->point.y - waypoints_->at(waypoints_->size() - 1)->position.y,
              msg->point.x - waypoints_->at(waypoints_->size() - 1)->position.x);
  } else {
    tf::Quaternion current_q(current_pose_.orientation.x,
        current_pose_.orientation.y,
        current_pose_.orientation.z,
        current_pose_.orientation.w);
    tf::Matrix3x3 m(current_q);
    m.getRPY(roll, pitch, yaw); 
  }
  q.setRPY(0.0, 0.0, yaw);  // Waypoints should be vertically assigned

  saved_point->position.x = msg->point.x;
  saved_point->position.y = msg->point.y;
  saved_point->position.z = msg->point.z;
  saved_point->orientation.x = q.x();  // current_pose_.orientation.x;
  saved_point->orientation.y = q.y();  // current_pose_.orientation.y;
  saved_point->orientation.z = q.z();  // current_pose_.orientation.z;
  saved_point->orientation.w = q.w();  // current_pose_.orientation.w;

  // export ROSCONSOLE_FORMAT='${message}'
  ROS_WARN("  - point: #%d", waypoints_->size() + 1);
  ROS_WARN("      x  : %f", saved_point->position.x);
  ROS_WARN("      y  : %f", saved_point->position.y);
  ROS_WARN("      th : %f", yaw);
  waypoints_->push_back(saved_point);
  robot_state_.goal_count = waypoints_->size();
  robot_state_pub.publish(robot_state_);
}

void BehaviorFlow() {
  if (action_client_movebase_->getState() ==
          actionlib::SimpleClientGoalState::REJECTED ||
      actionlib::SimpleClientGoalState::ABORTED ||
      actionlib::SimpleClientGoalState::LOST) {
    robot_state_saved_.robot_state = "IDLE";
  }
  if (robot_state_msg.robot_state != robot_state_old_.robot_state) {
    robot_state_old_.robot_state = robot_state_msg.robot_state;
    ROS_WARN("State changed to : %s", robot_state_msg.robot_state.c_str());
    if (robot_state_msg.robot_state == "AtoB" && joy_flag == true) {  // A to B state
      if(action_client_movebase_->getState() ==
                 actionlib::SimpleClientGoalState::ACTIVE) {
        ROS_WARN("Resuming previous state...");
        return;
      }
      ROS_INFO("Following waypoints");
      FollowWaypoints();
      joy_flag = false;
    } else if (robot_state_msg.robot_state == "AtoH") {  // A to Home state
      if(action_client_movebase_->getState() ==
                 actionlib::SimpleClientGoalState::ACTIVE) {
        ROS_WARN("Resuming previous state...");
        return;
      }
      ROS_INFO("Send home");
      SendHome();
    } else if (robot_state_msg.robot_state == "PATROL" && joy_flag == true) {  // Patrol state
      if(action_client_movebase_->getState() ==
                 actionlib::SimpleClientGoalState::ACTIVE) {
        ROS_WARN("Resuming previous state...");
        return;
      }
      ROS_INFO("Patroling");
      FollowWaypoints(); //FollowPath();
    } else if (robot_state_msg.robot_state == "HOLD") {  // hold
      ROS_INFO("Holding every function by sending 0 command velocity");
      HoldEveryFunction();
    } 
  } else if (robot_state_msg.robot_state == "PATROL" &&
             action_client_movebase_->getState() ==
                 actionlib::SimpleClientGoalState::SUCCEEDED && joy_flag == true) {  // patrol loop
    FollowWaypoints(); //FollowPath();
    ROS_INFO("Sending patrol route");
  } else if (robot_state_msg.robot_state == "AtoH" &&
             action_client_movebase_->getState() ==
                 actionlib::SimpleClientGoalState::SUCCEEDED) {  // Arrived home, initiate docking
    cmd_vel_.linear.x = 0.0;
    cmd_vel_.angular.z = 0.0;
    cmd_vel_pub.publish(cmd_vel_);
    sleep(2);
    robot_state_.robot_state = "BUMPER_RECOVERY";
    robot_state_pub.publish(robot_state_);
    sleep(2);
    robot_state_.robot_state = "Docking";
    robot_state_pub.publish(robot_state_);
    ROS_INFO("Arrived home, initiate docking");
  } else if (robot_state_msg.robot_state == "AtoB" && 
             action_client_movebase_->getState() ==
                 actionlib::SimpleClientGoalState::
                     SUCCEEDED) {  // setting back to idle when done
    robot_state_.robot_state = "IDLE";
    robot_state_pub.publish(robot_state_);
    ROS_INFO("Done current task, switching back to idle");
  } else if (robot_state_msg.robot_state == "Undocking") {
    ROS_INFO("Undocking initiated, wait for 5 seconds");
    cmd_vel_.linear.x = 0.1;
    cmd_vel_.angular.z = 0.0;
    cmd_vel_pub.publish(cmd_vel_);
    sleep(3);
    cmd_vel_.linear.x = 0.0;
    cmd_vel_.angular.z = 0.0;
    cmd_vel_pub.publish(cmd_vel_);
    robot_state_.robot_state = "BUMPER_UNLOCK";
    robot_state_pub.publish(robot_state_);
    sleep(1);
    robot_state_.robot_state = "IDLE";
    robot_state_pub.publish(robot_state_);
    ROS_INFO("Done undocking, switching back to idle");
  } else if (robot_state_msg.robot_state == "MANUAL") {
    robot_state_saved_.robot_state = robot_state_.robot_state;
    CancelFollowWaypoints();
  }
  sleep(1);
}

void joy_CB(const sensor_msgs::Joy joy_msg) {
  current_time = ros::Time::now();
  if (joy_msg.buttons[bumper_deactivate_num] && joy_msg.buttons[5] &&
             joy_msg.buttons[7] && (current_time - last_time > duration_min)) {
    robot_switch_.bumper_switch = 2;
    robot_switch_pub.publish(robot_switch_);
    last_time = current_time;
  } else if (joy_msg.buttons[bumper_activate_num] && joy_msg.buttons[5] &&
             joy_msg.buttons[7] && (current_time - last_time > duration_min)) {
    robot_switch_.bumper_switch = 0;
    robot_switch_pub.publish(robot_switch_);
    last_time = current_time;
  } else if (joy_msg.buttons[activate_num] && joy_msg.buttons[idle_num] &&
      (current_time - last_time > duration_min)) {
    robot_state_.robot_state = "IDLE";
    robot_state_pub.publish(robot_state_);
    last_time = current_time;
  } else if (joy_msg.buttons[front_light_num] && joy_msg.buttons[5] &&
             joy_msg.buttons[7] && (current_time - last_time > duration_min)) {
    if (robot_switch_.front_light_switch == 0) {
      robot_switch_.front_light_switch = 1;
    } else if (robot_switch_.front_light_switch == 1) {
      robot_switch_.front_light_switch = 0;
    }
    robot_switch_pub.publish(robot_switch_);
    last_time = current_time;
  } else if (joy_msg.buttons[water_pump_num] && joy_msg.buttons[5] &&
             joy_msg.buttons[7] && (current_time - last_time > duration_min)) {
    if (robot_switch_.water_pump_switch != 3) {
      robot_switch_.water_pump_switch++;
    } else if (robot_switch_.water_pump_switch == 3) {
      robot_switch_.water_pump_switch = 0;
    }
    robot_switch_pub.publish(robot_switch_);
    last_time = current_time;
  } else if (joy_msg.buttons[uvc_lamp_num] && joy_msg.buttons[5] &&
             joy_msg.buttons[7] && (current_time - last_time > duration_min)) {
    if (robot_switch_.uvc_lamp_switch != 3) {
      robot_switch_.uvc_lamp_switch++;
    } else if (robot_switch_.uvc_lamp_switch == 3) {
      robot_switch_.uvc_lamp_switch = 0;
    }
    robot_switch_pub.publish(robot_switch_);
    last_time = current_time;
  } else if (joy_msg.buttons[uva_lamp_num] && joy_msg.buttons[5] &&
             joy_msg.buttons[7] && (current_time - last_time > duration_min)) {
    if (robot_switch_.uva_lamp_switch != 3) {
      robot_switch_.uva_lamp_switch++;
    } else if (robot_switch_.uva_lamp_switch == 3) {
      robot_switch_.uva_lamp_switch = 0;
    }
    robot_switch_pub.publish(robot_switch_);
    last_time = current_time;
  } else if (joy_msg.buttons[linear_motor_num] && joy_msg.buttons[5] &&
             joy_msg.buttons[7] && (current_time - last_time > duration_min)) {
    if (robot_switch_.rear_light_switch != 4) {
      robot_switch_.rear_light_switch++;
    } else if (robot_switch_.rear_light_switch == 4) {
      robot_switch_.rear_light_switch = 0;
    }
    robot_switch_pub.publish(robot_switch_);
    last_time = current_time;
  } else if (joy_msg.buttons[activate_num] && joy_msg.buttons[atob_num] &&
             (current_time - last_time > duration_min)) {
    joy_flag = true;
    robot_state_.robot_state = "AtoB";
    robot_state_pub.publish(robot_state_);
    last_time = current_time;
  } else if (joy_msg.buttons[activate_num] && joy_msg.buttons[atoh_num] &&
             (current_time - last_time > duration_min)) {
    joy_flag = true;
    robot_state_.robot_state = "AtoH";
    robot_state_pub.publish(robot_state_);
    last_time = current_time;
  } else if (joy_msg.buttons[activate_num] && joy_msg.buttons[patrol_num] &&
             (current_time - last_time > duration_min)) {
    joy_flag = true;
    robot_state_.robot_state = "PATROL";
    robot_state_pub.publish(robot_state_);
    last_time = current_time;
  } else if (joy_msg.buttons[activate_num] && joy_msg.buttons[hold_num] &&
             (current_time - last_time > duration_min)) {
    robot_state_.robot_state = "HOLD";
    robot_state_pub.publish(robot_state_);
    last_time = current_time;
  } else if (joy_msg.buttons[activate_num] && joy_msg.buttons[manual_num] &&
             joy_msg.buttons[7] &&
             (current_time - last_time > duration_min)) {  // too much buttons!!
    robot_state_.robot_state = "MANUAL";
    robot_state_pub.publish(robot_state_);
    last_time = current_time;
  } else if (joy_msg.buttons[activate_waypoint_num] &&
             joy_msg.buttons[set_waypoint_num] &&
             (current_time - last_time > duration_min)) {
    ROS_INFO("Saving current location to waypoint");
    MakeCurrentPoseToWaypoint();
    robot_state_.goal_count = waypoints_->size();
    robot_state_pub.publish(robot_state_);
    last_time = current_time;
  } else if (joy_msg.buttons[activate_waypoint_num] &&
             joy_msg.buttons[remove_last_waypoint_num] &&
             (current_time - last_time > duration_min)) {
    RemoveLastWaypoint();
    robot_state_.goal_count = waypoints_->size();
    robot_state_pub.publish(robot_state_);
    last_time = current_time;
  } else if (joy_msg.buttons[activate_waypoint_num] &&
             joy_msg.buttons[clear_all_waypoints_num] &&
             (current_time - last_time > duration_min)) {
    RemoveAllWaypoint();
    robot_state_.goal_count = waypoints_->size();
    robot_state_pub.publish(robot_state_);
    last_time = current_time;
  } else if (joy_msg.buttons[activate_waypoint_num] &&
             joy_msg.buttons[cancel_waypoints_num] &&
             (current_time - last_time > duration_min)) {
    CancelFollowWaypoints();
    joy_flag = false;
    last_time = current_time;
  } else if (joy_msg.buttons[0] && joy_msg.buttons[3] &&
             (current_time - last_time > duration_min)) {
    if(clear_costmap_service_.call(srv)){
      ROS_INFO("Costmap cleared!!");
    }
    last_time = current_time;
  }
}

bool GetPathFromFile() {
  // clear way points vector
  path_->clear();
  try {
    // check file open
    std::ifstream ifs;
    if(robot_state_.robot_state == "AtoH") {
      ifs.open(home_path_filename_.c_str(), std::ifstream::in);
    } else ifs.open(path_filename_.c_str(), std::ifstream::in);
    if (!ifs.good()) {
      ROS_WARN("!good");
      return false;
    }
    // yaml node
    YAML::Node yaml_node;
    yaml_node = YAML::Load(ifs);
    const YAML::Node& wp_node_tmp = yaml_node["waypoints"];
    const YAML::Node* wp_node = wp_node_tmp ? &wp_node_tmp : NULL;

    if (wp_node != NULL) {
      // loop over all the waypoints
      for (int i = 0; i < wp_node->size(); i++) {
        // get each waypoint
        geometry_msgs::Pose* path_point = new geometry_msgs::Pose();

        (*wp_node)[i]["point"]["x"] >> path_point->position.x;
        (*wp_node)[i]["point"]["y"] >> path_point->position.y;
        (*wp_node)[i]["point"]["th"] >> path_point->orientation.z;

        // convert the degrees to quaternion
        double yaw = path_point->orientation.z * M_PI / 180.;
        tf2::Quaternion q;
        q.setRPY(0., 0., yaw);
        path_point->orientation.x = q.x();
        path_point->orientation.y = q.y();
        path_point->orientation.z = q.z();
        path_point->orientation.w = q.w();

        ROS_WARN("path received : %f, %f, %f", path_point->position.x,
                 path_point->position.y, path_point->orientation.z);

        path_->push_back(path_point);
      }
    } else {
      ROS_WARN("else");
      return false;
    }
  } catch (YAML::ParserException& e) {
    ROS_WARN("ParserException");
    return false;
  } catch (YAML::RepresentationException& e) {
    ROS_WARN("RepresentationException");
    return false;
  }
  return true;
}

void FollowPath() {
  // If the robot is moving for a action.
  if (action_client_movebase_->getState() ==
      actionlib::SimpleClientGoalState::ACTIVE) {
    ROS_WARN("Other action is active. Sending new points...");
  }
  goal.waypoints.clear();
  bool built = GetPathFromFile();
  if (!built) {
    ROS_FATAL("Building waypoints from a file failed");
    robot_state_.robot_state = "IDLE";
    robot_state_pub.publish(robot_state_);
    return;
  }
  if (path_->size() == 0) {
    ROS_FATAL("Path file is empty");
    robot_state_.robot_state = "IDLE";
    robot_state_pub.publish(robot_state_);
    return;
  }
  if (joy_flag == false) {
    ROS_FATAL("State has not been changed by joypad");
    robot_state_.robot_state = "IDLE";
    robot_state_pub.publish(robot_state_);
    return;
  }

  for (size_t i = 0; i < path_->size(); i++) {
    geometry_msgs::Pose waypoint;
    waypoint.position.x = path_->at(i)->position.x;
    waypoint.position.y = path_->at(i)->position.y;
    waypoint.orientation.z = path_->at(i)->orientation.z;
    goal.waypoints.push_back(waypoint);
  }
  action_client_movebase_->sendGoal(goal);
  action_thread_movebase = boost::thread(&CheckMoveBaseActionStatus);
}

void FollowWaypoints() {
  // If the robot is moving for a action.
  if (action_client_movebase_->getState() ==
      actionlib::SimpleClientGoalState::ACTIVE) {
    ROS_WARN("Other action is active. Sending new points...");
  }
  goal.waypoints.clear();

  dbot_msgs::DbotControlGoal goal;
  dbot_msgs::DrivingMode driving_mode;
  goal.driving_mode.driving_mode = 1;
  
  if (waypoints_->size() == 0) {
    ROS_FATAL("Waypoint list is empty, switching back to IDLE...");
    robot_state_.robot_state = "IDLE";
    robot_state_pub.publish(robot_state_);
    return;
  }
  if (joy_flag == false) {
    ROS_FATAL("State has not been changed by joypad");
    robot_state_.robot_state = "IDLE";
    robot_state_pub.publish(robot_state_);
    return;
  }

  for (size_t i = 0; i < waypoints_->size(); i++) {
    geometry_msgs::Pose waypoint;
    waypoint.position.x = waypoints_->at(i)->position.x;
    waypoint.position.y = waypoints_->at(i)->position.y;
    waypoint.orientation.x = waypoints_->at(i)->orientation.x;
    waypoint.orientation.y = waypoints_->at(i)->orientation.y;
    waypoint.orientation.z = waypoints_->at(i)->orientation.z;
    waypoint.orientation.w = waypoints_->at(i)->orientation.w;
    goal.waypoints.push_back(waypoint);
  }
  action_client_movebase_->sendGoal(goal);
  action_thread_movebase = boost::thread(&CheckMoveBaseActionStatus);
}

void CancelFollowWaypoints() {
  if (action_client_movebase_->getState() ==
      actionlib::SimpleClientGoalState::ACTIVE) {
    // If the robot is moving for movebase action
    ROS_WARN("Action MoveBase is canceled");
    action_client_movebase_->cancelAllGoals();
    StopThread(action_thread_movebase);
    robot_state_saved_.robot_state = robot_state_.robot_state;
    robot_state_.robot_state = "IDLE";
    robot_state_pub.publish(robot_state_);
    return;
  }
}

void HoldEveryFunction() {
  robot_state_saved_.robot_state = robot_state_.robot_state;
  hold_vel_.linear.x = 0;
  hold_vel_.angular.z = 0;
  hold_vel_pub.publish(hold_vel_);
}

void SendHome() {
  // If the robot is moving for a action.
  if (action_client_movebase_->getState() ==
      actionlib::SimpleClientGoalState::ACTIVE) {
    ROS_WARN("Canceled the current action. Sending home...");
  }

  dbot_msgs::DbotControlGoal home;
  dbot_msgs::DrivingMode driving_mode;
  goal.driving_mode.driving_mode = 1;

  geometry_msgs::Pose waypoint;

  // set home location
  waypoint.position.x = 0.0;
  waypoint.position.y = 0.0;
  waypoint.orientation.x = 0.0;
  waypoint.orientation.y = 0.0;
  waypoint.orientation.z = 0.0;
  waypoint.orientation.w = 1.0;
  home.waypoints.push_back(waypoint);
  action_client_movebase_->sendGoal(home);

  action_thread_movebase = boost::thread(&CheckMoveBaseActionStatus);
}

void MakeCurrentPoseToWaypoint() {
  geometry_msgs::Pose* current_point = new geometry_msgs::Pose();

  tf::Quaternion q(
      current_pose_.orientation.x,
      current_pose_.orientation.y,
      current_pose_.orientation.z,
      current_pose_.orientation.w);
  tf::Matrix3x3 m(q);
  double roll, pitch, yaw;
  m.getRPY(roll, pitch, yaw);
  q.setRPY(0.0, 0.0, yaw);              // Waypoints should be vertically assigned

  ROS_INFO("%f, %f, %f", roll, pitch, yaw);

  current_point->position.x = current_pose_.position.x;
  current_point->position.y = current_pose_.position.y;
  current_point->position.z = current_pose_.position.z;
  current_point->orientation.x = q.x(); //current_pose_.orientation.x;
  current_point->orientation.y = q.y(); //current_pose_.orientation.y;
  current_point->orientation.z = q.z(); //current_pose_.orientation.z;
  current_point->orientation.w = q.w(); //current_pose_.orientation.w;

  ROS_INFO("Saved point : %f, %f", current_point->position.x,
           current_point->position.y);
  waypoints_->push_back(current_point);
  ROS_INFO("Waypoint size : %d", waypoints_->size());
}

void CallBackCurrentPose(const geometry_msgs::Pose::ConstPtr& msg) {
  current_pose_.position.x = msg->position.x;
  current_pose_.position.y = msg->position.y;
  current_pose_.position.z = msg->position.z;

  current_pose_.orientation.x = msg->orientation.x;
  current_pose_.orientation.y = msg->orientation.y;
  current_pose_.orientation.z = msg->orientation.z;
  current_pose_.orientation.w = msg->orientation.w;
}

void RemoveAllWaypoint() {
  int num_waypoint = waypoints_->size();
  for (int i = num_waypoint - 1; i >= 0; i--) {
    ROS_INFO("%f, %f", waypoints_->at(i)->position.x,
             waypoints_->at(i)->position.y);
    waypoints_->pop_back();
  }
  ROS_INFO("Removed all waypoints");
}

void RemoveLastWaypoint() {
  int last_point_idx = waypoints_->size();
  if (last_point_idx == 0){
    ROS_WARN("Waypoint is already empty!");
  } else {
    ROS_INFO("Removed waypoint : %f, %f",
            waypoints_->at(last_point_idx - 1)->position.x,
            waypoints_->at(last_point_idx - 1)->position.y);
    waypoints_->pop_back();
  }
}

void CheckMoveBaseActionStatus() { action_client_movebase_->waitForResult(); }

double DistanceCalculatorFromCurrPose(double x, double y) {
  return sqrt(pow(current_pose_.position.x - x, 2) +
              pow(current_pose_.position.y - y, 2) * 1.0);
}

void StopThread(boost::thread& target_thread) {
  if (target_thread.joinable()) {
    target_thread.join();
  }
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "dbot_behavior_flow_node");
  ros::NodeHandle nh;

  current_time = ros::Time::now();
  last_time = ros::Time::now();

  clear_current_time = ros::Time::now();
  clear_last_time = ros::Time::now();

  joy_sub = nh.subscribe("/joy", 100, joy_CB);
  robot_state_sub = nh.subscribe("/robot_state", 0, SubRobotState);
  robot_status_sub = nh.subscribe("/robot_status", 0, SubRobotStatus);
  robot_switch_sub = nh.subscribe("/robot_switch", 0, SubRobotSwitch);
  constraint_sub = nh.subscribe("/constraint_list", 0, SubConstraintList);
  clicked_point_sub = nh.subscribe("/clicked_point", 0, SubClickedPoint);
  robot_state_pub = nh.advertise<dbot_msgs::RobotState>("robot_state", 20);
  robot_status_pub = nh.advertise<dbot_msgs::RobotStatus>("robot_status", 20);
  robot_switch_pub = nh.advertise<dbot_msgs::RobotSwitch>("robot_switch", 20);
  hold_vel_pub = nh.advertise<geometry_msgs::Twist>("hold_vel", 20);
  cmd_vel_pub = nh.advertise<geometry_msgs::Twist>("cmd_vel", 20);
  current_pose_sub_ = nh.subscribe("/robot_pose", 1, CallBackCurrentPose);
  nh.param<std::string>("path_filename", params_.path_filename,
                        "path/path.yaml");
  nh.param<std::string>("home_path_filename", params_.home_path_filename,
                        "path/home_path.yaml");
  std::string pkg_path = ros::package::getPath("dbot_behavior_flow");
  path_filename_ = pkg_path + "/" + params_.path_filename;
  home_path_filename_ = pkg_path + "/" + params_.home_path_filename;

  clear_costmap_service_ = nh.serviceClient<std_srvs::Empty>("move_base/clear_costmaps");

  waypoints_ = new std::vector<geometry_msgs::Pose*>();
  path_ = new std::vector<geometry_msgs::Pose*>();
  // dbot_movebase Action client initializing
  action_client_movebase_ =
      new actionlib::SimpleActionClient<dbot_msgs::DbotControlAction>(
          "dbot_movebase_action", true);

  robot_switch_.front_light_switch = 0;
  robot_switch_.water_pump_switch = 0;
  robot_switch_.uvc_lamp_switch = 0;
  robot_switch_.uva_lamp_switch = 0;
  robot_switch_.linear_motor_switch = 0;
  robot_switch_.rear_light_switch = 0;
  
  robot_state_.robot_state = "IDLE";
  robot_state_saved_.robot_state = "IDLE";
  robot_state_old_.robot_state = "IDLE";
  robot_state_pub.publish(robot_state_);
  robot_status_.docking_mode = 1;
  robot_status_pub.publish(robot_status_);
  ros::Rate r(20.0);
  while (nh.ok()) {
    clear_current_time = ros::Time::now();
    if (robot_state_.robot_state == "HOLD") HoldEveryFunction();
    if ((clear_current_time - clear_last_time > clear_duration_min)) { // clear costmap in every sec
      if(clear_costmap_service_.call(srv)){
        // ROS_INFO("Costmap cleared!!");
        clear_last_time = clear_current_time;
      }
    }
    BehaviorFlow();
    ros::spinOnce();  // check for incoming messages
    r.sleep();
  }
}