#include <cmath>
#include <geometry_msgs/Twist.h>
#include <ros/ros.h>
#include <yaml-cpp/yaml.h>
#include <fstream>
#include <tf/tf.h>
#include <math.h>

ros::Publisher out_vel_pub_;

ros::Subscriber cmd_vel_sub_;
ros::Subscriber joy_vel_sub_;
ros::Subscriber hold_vel_sub_;
ros::Subscriber key_vel_sub_;
ros::Subscriber flt_vel_sub_;
geometry_msgs::Twist cmd_vel_;
geometry_msgs::Twist joy_vel_;
geometry_msgs::Twist hold_vel_;
geometry_msgs::Twist key_vel_;
geometry_msgs::Twist flt_vel_;
geometry_msgs::Twist out_vel_;

void cmdVelCallback(const geometry_msgs::Twist& msg) {
  cmd_vel_ = msg;
}

void joyVelCallback(const geometry_msgs::Twist& msg) {
  joy_vel_ = msg;
}

void holdVelCallback(const geometry_msgs::Twist& msg) {
  hold_vel_ = msg;
}

void fltVelCallback(const geometry_msgs::Twist& msg) {
  flt_vel_ = msg;
}

void keyVelCallback(const geometry_msgs::Twist& msg) {
  key_vel_ = msg;
}

void velocityMuxer() {
  if (key_vel_.linear.x !=  0.0 || key_vel_.angular.z != 0.0 ){
    out_vel_ = key_vel_;
  }
  if (flt_vel_.linear.x !=  0.0 || flt_vel_.angular.z != 0.0 ){
    out_vel_ = flt_vel_;
  }
  if (cmd_vel_.linear.x !=  0.0 || cmd_vel_.angular.z != 0.0 ){
    out_vel_ = cmd_vel_;
  }
  if (hold_vel_.linear.x !=  0.0 || hold_vel_.angular.z != 0.0 ){
    out_vel_ = hold_vel_;
  }
  if (joy_vel_.linear.x !=  0.0 || joy_vel_.angular.z != 0.0 ){
    out_vel_ = joy_vel_;
  }

  out_vel_pub_.publish(out_vel_);
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "dbot_vel_mux_node");
  ros::NodeHandle nh = ros::NodeHandle(), private_nh = ros::NodeHandle("~");

  cmd_vel_sub_ = nh.subscribe("/cmd_vel", 1, cmdVelCallback);
  joy_vel_sub_ = nh.subscribe("/joy_vel", 1, joyVelCallback);
  hold_vel_sub_ = nh.subscribe("/hold_vel", 1, holdVelCallback);
  flt_vel_sub_ = nh.subscribe("/flt_vel", 1, fltVelCallback);
  key_vel_sub_ = nh.subscribe("/key_vel", 1, keyVelCallback);

  out_vel_pub_ = nh.advertise<geometry_msgs::Twist>("/out_vel", 1);

  velocityMuxer();

  ros::spin();

  return 0;
}