#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>
#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/Range.h>
#include <tf/transform_broadcaster.h>
#include <visualization_msgs/MarkerArray.h>

#include "dbot_msgs/RobotStatus.h"
#include "dbot_msgs/RobotSwitch.h"

ros::Subscriber mux_vel_sub;
ros::Subscriber imu_sub;
ros::Subscriber scan_sub;
ros::Subscriber status_sub;
ros::Subscriber switch_sub;
ros::Publisher out_vel_pub;
ros::Publisher odom_pub;
ros::Publisher ultrasonic1_pub;
ros::Publisher ultrasonic2_pub;
ros::Publisher ultrasonic3_pub;
ros::Publisher ultrasonic4_pub;
ros::Publisher ultrasonic5_pub;
ros::Publisher ultrasonic6_pub;
ros::Publisher ultrasonic7_pub;
ros::Publisher ultrasonic8_pub;
ros::Publisher crash_marker_pub;

geometry_msgs::Twist mux_vel_msg;
geometry_msgs::Twist out_vel_;
sensor_msgs::Imu imu_msg;
sensor_msgs::LaserScan scan_msg;
dbot_msgs::RobotStatus robot_status_msg;
dbot_msgs::RobotSwitch robot_switch_msg;
visualization_msgs::MarkerArray crash_marker_array;
visualization_msgs::Marker crash_marker;

// variables for motion control and acc
float v_ref, w_ref = 0;
float v_tar, w_tar = 0;
float v_cmd, w_cmd = 0;
float v_cur, w_cur = 0;
float kv_vel = 0.25;
float kw_vel = 0.25;
float kv = 0.05;
float kw = 0.10;
float max_vel = 1.0;
bool vel_control = 1;
bool acceleration_mode = 1;
bool enable_collision_detection = 1;

// variables for odom
double x = 0.0;
double y = 0.0;
double th = 0.0;

double vx = 0.0;
double vy = 0.0;
double vth = 0.0;

int crash = 0;

ros::Time current_time, last_time;

void SubMuxVel(const geometry_msgs::Twist& msg) {
  mux_vel_msg = msg;
  return;
}

void SubImu(const sensor_msgs::Imu& msg) {
  imu_msg = msg;
  return;
}

void SubScan(const sensor_msgs::LaserScan& msg) {
  scan_msg = msg;
  return;
}

void SubRobotStatus(const dbot_msgs::RobotStatus& msg) {
  robot_status_msg = msg;
  return;
}

void SubRobotSwitch(const dbot_msgs::RobotSwitch& msg) {
  robot_switch_msg = msg;
  return;
}

void CollisionDetection() {
  crash_marker_array.markers.clear();
  crash_marker.header.frame_id = "velodyne";
  crash_marker.header.stamp = ros::Time();
  crash_marker.ns = "crash_marker";
  crash_marker.type = visualization_msgs::Marker::SPHERE;
  crash_marker.action = visualization_msgs::Marker::ADD;
  crash_marker.pose.position.x = 0.0;
  crash_marker.pose.position.y = 0.0;
  crash_marker.pose.position.z = 0.0;
  crash_marker.pose.orientation.x = 0.0;
  crash_marker.pose.orientation.y = 0.0;
  crash_marker.pose.orientation.z = 0.0;
  crash_marker.pose.orientation.w = 1.0;
  crash_marker.color.a = 1.0; // Don't forget to set the alpha!
  // simulation parameter setting & mem alloc
  double ts = 0.1;   //simulation sampling time
  double fint = 1.5;   //simulation finit time
  int lent = fint / ts;   // simulation time array size
  crash = 0;

  double rx[lent] = {};
  double ry[lent] = {};
  double rth[lent] = {};

  double robot_shape_min_x = -0.50;   // robot size 
  double robot_shape_min_y = -0.30;
  double robot_shape_max_x = 0.10;
  double robot_shape_max_y = 0.30;

  int robot_shape_y_ori_size = int((robot_shape_max_y - robot_shape_min_y) / 0.05);
  int robot_shape_x_ori_size = int((robot_shape_max_x - robot_shape_min_x) / 0.05);

  double robot_shape_y_ori[robot_shape_y_ori_size] = {}; // setting the size of the robot surface
  double robot_shape_x_ori[robot_shape_x_ori_size] = {};

  double robot_shape_y_ori_increment = robot_shape_min_y;
  double robot_shape_x_ori_increment = robot_shape_min_x;

  for (int i = 0; i < robot_shape_y_ori_size; i++ ){
    robot_shape_y_ori[i] = robot_shape_y_ori_increment;
    robot_shape_y_ori_increment += 0.05;
  }
  for (int i = 0; i < robot_shape_x_ori_size; i++ ){
    robot_shape_x_ori[i] = robot_shape_x_ori_increment;
    robot_shape_x_ori_increment += 0.05;
  }

  double angle_min= -1.57; //-3.14159;
  double angle_max= 1.57; //3.141592;
  double angle_increment= 0.0087;

  double angle[scan_msg.ranges.size()] = {};
  double px[scan_msg.ranges.size()] = {};
  double py[scan_msg.ranges.size()] = {};

  double threshold_front_x = 0;
  double threshold_back_x = 0;
  double threshold_y = 0;

  int x_pos_local_map = 0;
  int y_pos_local_map = 0;

  for (int i = 0; i < scan_msg.ranges.size(); i++){
    if(scan_msg.ranges[i] > 0.4){
      angle[i] = angle_min + angle_increment*(i-1);
      px[i] = scan_msg.ranges[i] * cos(angle[i]);
      py[i] = scan_msg.ranges[i] * sin(angle[i]);
      // pre check collision (considering movement strait forward)
      threshold_front_x = 0.1 + robot_shape_max_x;
      threshold_back_x = 0.1;
      threshold_y = 0.1 + robot_shape_max_y;
      // if (px[i] > robot_shape_max_x && px[i] < threshold_front_x){// Check if there are obstacles ahead
      //   if (px[i] > -threshold_y && py[i] < threshold_y) crash = 1;
      // }
    }
  }

  if (!crash){ 
    //// local_map building
    x_pos_local_map = int(10 / 0.05);
    y_pos_local_map = int(10 / 0.05); // local map size : +/-5m, cellsize : 5cm

    int local_map[x_pos_local_map][y_pos_local_map] = {};

    for (int i = 0; i < scan_msg.ranges.size(); i++){
      if (px[i]>-5 && px[i]<5 && py[i]>-5 && py[i]<5 && px[i] != 0 && py[i] != 0){
        int obx = round((px[i]+5)*20+1);
        int oby = round((py[i]+5)*20+1);
        local_map[oby][obx] = 1;
        crash_marker.id = i+2000;
        crash_marker.pose.position.x = (double(obx)/20 - 5);
        crash_marker.pose.position.y = (double(oby)/20 - 5);
        crash_marker.scale.x = 0.05;
        crash_marker.scale.y = 0.05;
        crash_marker.scale.z = 0.05;
        crash_marker.color.r = 1.0;
        crash_marker.color.g = 0.0;
        crash_marker.color.b = 0.0;
        crash_marker_array.markers.push_back(crash_marker);
      }
    }
    crash_marker.scale.x = 0.1;
    crash_marker.scale.y = 0.1;
    crash_marker.scale.z = 0.1;

    // check collision
    rx[0] = 0;
    ry[0] = 0;
    rth[0] = 0; // initialize robot location

    for (int i = 0; i < lent-1; i++){
      rx[i+1] = rx[i] + mux_vel_msg.linear.x*cos(rth[i])*ts; // estimated robot path
      ry[i+1] = ry[i] + mux_vel_msg.linear.x*sin(rth[i])*ts;
      if (mux_vel_msg.linear.x > 0){
        // ROS_WARN("%f", rth[i] + mux_vel_msg.angular.z*ts);
        rth[i+1] = rth[i] + mux_vel_msg.angular.z*ts;
      } else if (mux_vel_msg.linear.x < 0){
        // ROS_WARN("%f", rth[i] - mux_vel_msg.angular.z*ts);
        rth[i+1] = rth[i] - mux_vel_msg.angular.z*ts;
      }

      int robot_shape_front_x_img[robot_shape_y_ori_size] = {};
      int robot_shape_front_y_img[robot_shape_y_ori_size] = {};
      int robot_shape_back_x_img[robot_shape_y_ori_size] = {};
      int robot_shape_back_y_img[robot_shape_y_ori_size] = {};
      int robot_shape_left_x_img[robot_shape_x_ori_size] = {};
      int robot_shape_left_y_img[robot_shape_x_ori_size] = {};
      int robot_shape_right_x_img[robot_shape_x_ori_size] = {};
      int robot_shape_right_y_img[robot_shape_x_ori_size] = {};

      int rx_img[lent] = {};
      int ry_img[lent] = {};

      rx_img[i] = round((rx[i]+5)*20+1); // robot path on the map
      ry_img[i] = round((ry[i]+5)*20+1);

      crash_marker.id = i;
      crash_marker.pose.position.x = (double(rx_img[i])/20 - 5);
      crash_marker.pose.position.y = (double(ry_img[i])/20 - 5);
      crash_marker.color.r = 0.0;
      crash_marker.color.g = 1.0;
      crash_marker.color.b = 0.0;
      crash_marker_array.markers.push_back(crash_marker);

      for (int j = 0; j < round((robot_shape_max_y - robot_shape_min_y)/0.05); j++){
        robot_shape_front_x_img[j] = round((( cos(rth[i])*robot_shape_max_x - sin(rth[i])*robot_shape_y_ori[j] +rx[i])+5)*20+1);// front surface
        robot_shape_front_y_img[j] = round((( sin(rth[i])*robot_shape_max_x + cos(rth[i])*robot_shape_y_ori[j] +ry[i])+5)*20+1);

        robot_shape_back_x_img[j] = round(((cos(rth[i])*robot_shape_min_x-sin(rth[i])*robot_shape_y_ori[j] +rx[i])+5)*20+1); // back surface
        robot_shape_back_y_img[j] = round(((sin(rth[i])*robot_shape_min_x+cos(rth[i])*robot_shape_y_ori[j] +ry[i])+5)*20+1);

        crash_marker.id = j+8000;
        crash_marker.pose.position.x = (double(robot_shape_front_x_img[j])/20 - 5);
        crash_marker.pose.position.y = (double(robot_shape_front_y_img[j])/20 - 5);
        crash_marker.color.r = 1.0;
        crash_marker.color.g = 0.0;
        crash_marker.color.b = 1.0;
        crash_marker_array.markers.push_back(crash_marker);
        crash_marker.id = j+8100;
        crash_marker.pose.position.x = (double(robot_shape_back_x_img[j])/20 - 5);
        crash_marker.pose.position.y = (double(robot_shape_back_y_img[j])/20 - 5);
        crash_marker.color.r = 1.0;
        crash_marker.color.g = 0.0;
        crash_marker.color.b = 1.0;
        crash_marker_array.markers.push_back(crash_marker);
        crash_marker.id = i+8200;
        crash_marker.pose.position.x = (double(robot_shape_front_x_img[j])/20 - 5);
        crash_marker.pose.position.y = (double(robot_shape_front_y_img[j])/20 - 5);
        crash_marker.color.r = 1.0;
        crash_marker.color.g = 0.0;
        crash_marker.color.b = 1.0;
        crash_marker_array.markers.push_back(crash_marker);
        crash_marker.id = i+8300;
        crash_marker.pose.position.x = (double(robot_shape_back_x_img[j])/20 - 5);
        crash_marker.pose.position.y = (double(robot_shape_back_y_img[j])/20 - 5);
        crash_marker.color.r = 1.0;
        crash_marker.color.g = 0.0;
        crash_marker.color.b = 1.0;
        crash_marker_array.markers.push_back(crash_marker);

        if (local_map[robot_shape_front_y_img[j]][robot_shape_front_x_img[j]] == 1) { // check front collision
            crash = 1;
        // ROS_WARN("front collision detected");
        crash_marker.id = j+4000;
        crash_marker.pose.position.x = (double(robot_shape_front_x_img[j])/20 - 5);
        crash_marker.pose.position.y = (double(robot_shape_front_y_img[j])/20 - 5);
        crash_marker.color.r = 0.0;
        crash_marker.color.g = 0.0;
        crash_marker.color.b = 1.0;
        crash_marker_array.markers.push_back(crash_marker);
        } 
        else if (local_map[robot_shape_back_y_img[j]][robot_shape_back_x_img[j]] == 1) { // check back collision
            crash = 1;
        // ROS_WARN("back collision detected");
        crash_marker.id = j+4000;
        crash_marker.pose.position.x = (double(robot_shape_back_x_img[j])/20 - 5);
        crash_marker.pose.position.y = (double(robot_shape_back_y_img[j])/20 - 5);
        crash_marker.color.r = 0.0;
        crash_marker.color.g = 0.0;
        crash_marker.color.b = 1.0;
        crash_marker_array.markers.push_back(crash_marker);
        }
      }

      for (int j = 0; j < round((robot_shape_max_x - robot_shape_min_x)/0.05)+1; j++){
        robot_shape_left_x_img[j] = round(((cos(rth[i])*robot_shape_x_ori[j]-sin(rth[i])*robot_shape_max_y +rx[i])+5)*20+1);
        robot_shape_left_y_img[j] = round(((sin(rth[i])*robot_shape_x_ori[j]+cos(rth[i])*robot_shape_max_y +ry[i])+5)*20+1);

        robot_shape_right_x_img[j] = round(((cos(rth[i])*robot_shape_x_ori[j]-sin(rth[i])*robot_shape_min_y +rx[i])+5)*20+1);
        robot_shape_right_y_img[j] = round(((sin(rth[i])*robot_shape_x_ori[j]+cos(rth[i])*robot_shape_min_y +ry[i])+5)*20+1);

        crash_marker.id = j+8400;
        crash_marker.pose.position.x = (double(robot_shape_left_x_img[j])/20 - 5);
        crash_marker.pose.position.y = (double(robot_shape_left_y_img[j])/20 - 5);
        crash_marker.color.r = 1.0;
        crash_marker.color.g = 1.0;
        crash_marker.color.b = 0.0;
        crash_marker_array.markers.push_back(crash_marker);
        crash_marker.id = j+8500;
        crash_marker.pose.position.x = (double(robot_shape_right_x_img[j])/20 - 5);
        crash_marker.pose.position.y = (double(robot_shape_right_y_img[j])/20 - 5);
        crash_marker.color.r = 1.0;
        crash_marker.color.g = 1.0;
        crash_marker.color.b = 0.0;
        crash_marker_array.markers.push_back(crash_marker);
        crash_marker.id = i+8600;
        crash_marker.pose.position.x = (double(robot_shape_left_x_img[j])/20 - 5);
        crash_marker.pose.position.y = (double(robot_shape_left_y_img[j])/20 - 5);
        crash_marker.color.r = 1.0;
        crash_marker.color.g = 1.0;
        crash_marker.color.b = 0.0;
        crash_marker_array.markers.push_back(crash_marker);
        crash_marker.id = i+8700;
        crash_marker.pose.position.x = (double(robot_shape_right_x_img[j])/20 - 5);
        crash_marker.pose.position.y = (double(robot_shape_right_y_img[j])/20 - 5);
        crash_marker.color.r = 1.0;
        crash_marker.color.g = 1.0;
        crash_marker.color.b = 0.0;
        crash_marker_array.markers.push_back(crash_marker);

        if (local_map[robot_shape_left_y_img[j]][robot_shape_left_x_img[j]]==1) { // check left collision
            crash = 1;
        // ROS_WARN("left collision detected");
        crash_marker.id = j+6000;
        crash_marker.pose.position.x = (double(robot_shape_left_x_img[j])/20 - 5);
        crash_marker.pose.position.y = (double(robot_shape_left_y_img[j])/20 - 5);
        crash_marker.color.r = 0.0;
        crash_marker.color.g = 1.0;
        crash_marker.color.b = 1.0;
        crash_marker_array.markers.push_back(crash_marker);
        } else if (local_map[robot_shape_right_y_img[j]][robot_shape_right_x_img[j]]==1) { // check right collision
            crash = 1;
        // ROS_WARN("right collision detected");
        crash_marker.id = j+6000;
        crash_marker.pose.position.x = (double(robot_shape_right_x_img[j])/20 - 5);
        crash_marker.pose.position.y = (double(robot_shape_right_y_img[j])/20 - 5);
        crash_marker.color.r = 0.0;
        crash_marker.color.g = 1.0;
        crash_marker.color.b = 1.0;
        crash_marker_array.markers.push_back(crash_marker);
        }
      }
    }
  }
  crash_marker_pub.publish( crash_marker_array );
}

void MotionAccControl(const geometry_msgs::Twist& msg,
                      const sensor_msgs::Imu& msg_imu_sub) {
  // Wook Velocity control ==========================================
  if (vel_control) {
    v_tar = msg.linear.x;
    w_tar = msg.angular.z;

    v_ref = msg.linear.x;
    w_ref = msg.angular.z;

    v_cur = msg.linear.x;  // to do : change value to motor driver odom
    w_cur = msg_imu_sub.angular_velocity.z;  // Xsens

    v_tar =
        v_tar +
        kv_vel *
            (v_ref -
             v_cur);  // v_cur and w_cur is current linear and angular velocity
                      // measured from motor driver and xsense, respectively.
    if (msg.linear.x == 0.0 && msg.angular.z == 0.0) {
      w_tar = msg.angular.z;
    } else
      w_tar = w_tar + kw_vel * (w_ref - w_cur);
  }
  //-----------------------------------------------------------------

  // Wook acceleration ============================================
  if (acceleration_mode) {
    if (!vel_control) {
      v_tar = msg.linear.x;
      w_tar = msg.angular.z;
    }
    if(crash){
      v_tar = 0.0;
      w_tar = 0.0;
    }
    // for(int i = 0; i < scan_msg.ranges.size(); i++){
    //   if(scan_msg.ranges[i] > 0.4 && scan_msg.ranges[i] < 0.7){
    //     ROS_WARN("Obstacle detected within 0.5m radius!!!");
    //     v_tar = 0.0;
    //     w_tar = 0.0;
    //   }
    // }
    if (1) {
      float v_lin = v_cmd + 0 * (v_tar - v_cmd) + kv * tanh(v_tar - v_cmd);
      float trap_v = 0;
      float slope = 0.03;
      if (v_cmd > 1.0) slope = 0.02;
      if (robot_switch_msg.start_stop_following_flag) slope = 0.02;
      // if (abs(v_tar - v_cmd) > 0.6) slope = 0.03;
      // if (abs(v_tar - v_cmd) > 1.2) slope = 0.01;
      if (v_tar > v_cmd) {
        trap_v = v_cmd + max_vel * slope;
      } else if (v_tar < v_cmd) {
        trap_v = v_cmd - max_vel * slope;
      } else trap_v = v_tar;
      float mu_input = abs(v_tar - v_cmd);
      float sigma = 0.1;
      float mu = exp(-(mu_input*mu_input)/(sigma*sigma));
      v_cmd = mu * v_lin + (1-mu) * trap_v;
    }
    if (0) {
      if (v_cmd > v_tar) v_cmd = v_cmd - 0.01;
      if (v_cmd < v_tar) v_cmd = v_cmd + 0.01;
      if (v_cmd == v_tar) v_cmd = v_tar;
    }
    if (v_tar == 0 && abs(v_cmd) < 0.01) v_cmd = 0.0;
    w_cmd = w_cmd + 2 * kw * (w_tar - w_cmd);  // wook incuded
    if (w_tar == 0 && abs(w_cmd) < 0.005) w_cmd = 0;
    // w_cmd = w_tar;  //wook included
    out_vel_.linear.x = v_cmd;
    out_vel_.angular.z = w_cmd;
    // out_vel_.angular.z = msg.angular.z;
    out_vel_pub.publish(out_vel_);
  }
  return;
}

void OdomPub(const dbot_msgs::RobotStatus& msg) {
  vx = robot_status_msg.v_enc;
  vy = 0;
  // vth = robot_status_msg.w_enc;
  vth = imu_msg.angular_velocity.z;
  current_time = ros::Time::now();

  if(abs(vth) < 0.03 && vx == 0.0) vth = 0.0;

  // compute odometry in a typical way given the velocities of the robot
  double dt = (current_time - last_time).toSec();
  double delta_x = (vx * cos(th) - vy * sin(th)) * dt;
  double delta_y = (vx * sin(th) + vy * cos(th)) * dt;
  double delta_th = vth * dt;

  x += delta_x;
  y += delta_y;
  th += delta_th;

  // since all odometry is 6DOF we'll need a quaternion created from yaw
  geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(th);

  // publish the odometry message over ROS
  nav_msgs::Odometry odom;
  odom.header.stamp = current_time;
  odom.header.frame_id = "odom";

  // set the position
  odom.pose.pose.position.x = x;
  odom.pose.pose.position.y = y;
  odom.pose.pose.position.z = 0.0;
  odom.pose.pose.orientation = odom_quat;

  // set the velocity
  odom.child_frame_id = "base_link";
  odom.twist.twist.linear.x = vx;
  odom.twist.twist.linear.y = vy;
  odom.twist.twist.angular.z = vth;

  // publish the message
  odom_pub.publish(odom);

  last_time = current_time;
  return;
}

void UltrasonicPub(const dbot_msgs::RobotStatus& msg) {
  current_time = ros::Time::now();

  sensor_msgs::Range ultrasonic1;
  sensor_msgs::Range ultrasonic2;
  sensor_msgs::Range ultrasonic3;
  sensor_msgs::Range ultrasonic4;
  sensor_msgs::Range ultrasonic5;
  sensor_msgs::Range ultrasonic6;
  sensor_msgs::Range ultrasonic7;
  sensor_msgs::Range ultrasonic8;

  ultrasonic1.header.stamp = current_time;
  ultrasonic2.header.stamp = current_time;
  ultrasonic3.header.stamp = current_time;
  ultrasonic4.header.stamp = current_time;
  ultrasonic5.header.stamp = current_time;
  ultrasonic6.header.stamp = current_time;
  ultrasonic7.header.stamp = current_time;
  ultrasonic8.header.stamp = current_time;
  ultrasonic1.header.frame_id = "ultrasonic1";
  ultrasonic2.header.frame_id = "ultrasonic2";
  ultrasonic3.header.frame_id = "ultrasonic3";
  ultrasonic4.header.frame_id = "ultrasonic4";
  ultrasonic5.header.frame_id = "ultrasonic5";
  ultrasonic6.header.frame_id = "ultrasonic6";
  ultrasonic7.header.frame_id = "ultrasonic7";
  ultrasonic8.header.frame_id = "ultrasonic8";

  ultrasonic1.radiation_type = 0;
  ultrasonic2.radiation_type = 0;
  ultrasonic3.radiation_type = 0;
  ultrasonic4.radiation_type = 0;
  ultrasonic5.radiation_type = 0;
  ultrasonic6.radiation_type = 0;
  ultrasonic7.radiation_type = 0;
  ultrasonic8.radiation_type = 0;
  ultrasonic1.field_of_view = 0.5236;
  ultrasonic2.field_of_view = 0.5236;
  ultrasonic3.field_of_view = 0.5236;
  ultrasonic4.field_of_view = 0.5236;
  ultrasonic5.field_of_view = 0.5236;
  ultrasonic6.field_of_view = 0.5236;
  ultrasonic7.field_of_view = 0.5236;
  ultrasonic8.field_of_view = 0.5236;

  ultrasonic1.range = double(robot_status_msg.ultrasonic_1) / 100;
  ultrasonic2.range = double(robot_status_msg.ultrasonic_2) / 100;
  ultrasonic3.range = double(robot_status_msg.ultrasonic_3) / 100;
  ultrasonic4.range = double(robot_status_msg.ultrasonic_4) / 100;
  ultrasonic5.range = double(robot_status_msg.ultrasonic_5) / 100;
  ultrasonic6.range = double(robot_status_msg.ultrasonic_6) / 100;
  ultrasonic7.range = double(robot_status_msg.ultrasonic_7) / 100;
  ultrasonic8.range = double(robot_status_msg.ultrasonic_8) / 100;

  ultrasonic1.max_range = 0.4;
  ultrasonic2.max_range = 0.4;
  ultrasonic3.max_range = 0.4;
  ultrasonic4.max_range = 0.4;
  ultrasonic5.max_range = 0.4;
  ultrasonic6.max_range = 0.4;
  ultrasonic7.max_range = 0.4;
  ultrasonic8.max_range = 0.4;
  ultrasonic1.min_range = 0.02;
  ultrasonic2.min_range = 0.02;
  ultrasonic3.min_range = 0.02;
  ultrasonic4.min_range = 0.02;
  ultrasonic5.min_range = 0.02;
  ultrasonic6.min_range = 0.02;
  ultrasonic7.min_range = 0.02;
  ultrasonic8.min_range = 0.02;

  ultrasonic1_pub.publish(ultrasonic1);
  ultrasonic2_pub.publish(ultrasonic2);
  ultrasonic3_pub.publish(ultrasonic3);
  ultrasonic4_pub.publish(ultrasonic4);
  ultrasonic5_pub.publish(ultrasonic5);
  ultrasonic6_pub.publish(ultrasonic6);
  ultrasonic7_pub.publish(ultrasonic7);
  ultrasonic8_pub.publish(ultrasonic8);
  
  return;
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "dbot_actuation_layer_node");

  ros::NodeHandle nh;

  nh.getParam("enable_collision_detection",enable_collision_detection);

  mux_vel_sub = nh.subscribe("/mux_vel", 0, SubMuxVel);
  imu_sub = nh.subscribe("/imu/data", 0, SubImu);
  scan_sub = nh.subscribe("/scan", 0, SubScan);
  status_sub = nh.subscribe("/robot_status", 0, SubRobotStatus);
  switch_sub = nh.subscribe("/robot_switch", 0, SubRobotSwitch);
  odom_pub = nh.advertise<nav_msgs::Odometry>("/dogu/odom", 20);
  // ultrasonic1_pub = nh.advertise<sensor_msgs::Range>("ultrasonic1", 20);
  // ultrasonic2_pub = nh.advertise<sensor_msgs::Range>("ultrasonic2", 20);
  // ultrasonic3_pub = nh.advertise<sensor_msgs::Range>("ultrasonic3", 20);
  // ultrasonic4_pub = nh.advertise<sensor_msgs::Range>("ultrasonic4", 20);
  // ultrasonic5_pub = nh.advertise<sensor_msgs::Range>("ultrasonic5", 20);
  // ultrasonic6_pub = nh.advertise<sensor_msgs::Range>("ultrasonic6", 20);
  // ultrasonic7_pub = nh.advertise<sensor_msgs::Range>("ultrasonic7", 20);
  // ultrasonic8_pub = nh.advertise<sensor_msgs::Range>("ultrasonic8", 20);
  crash_marker_pub = nh.advertise<visualization_msgs::MarkerArray>("crash_marker", 20);
  out_vel_pub = nh.advertise<geometry_msgs::Twist>("out_vel", 20);

  current_time = ros::Time::now();
  last_time = ros::Time::now();

  ros::Rate r(30.0);
  while (nh.ok()) {
    ros::spinOnce();  // check for incoming messages
    // if(enable_collision_detection) CollisionDetection();
    MotionAccControl(mux_vel_msg, imu_msg);
    OdomPub(robot_status_msg);
    // UltrasonicPub(robot_status_msg);
    r.sleep();
  }
}