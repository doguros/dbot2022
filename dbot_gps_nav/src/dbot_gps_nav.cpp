#include <cmath>
#include <geometry_msgs/Transform.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/QuaternionStamped.h>
#include <ros/ros.h>
#include <ros/package.h>
#include <sensor_msgs/Joy.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/Imu.h>
#include <nav_msgs/Odometry.h>
#include <robot_localization/navsat_conversions.h>
#include <geometry_msgs/PointStamped.h>
#include <yaml-cpp/yaml.h>
#include <iostream>
#include <fstream>
#include <tf/tf.h>
#include <math.h>
#include "dbot_msgs/RobotStatus.h"
#include "dbot_msgs/RobotSwitch.h"
#include "dbot_gps_nav/NavStatus.h"

#define DEG2RAD(angle_degree) (angle_degree * M_PI / 180.0)
#define RAD2DEG(angle_radian) (angle_radian * 180.0 / M_PI)

ros::Publisher cmd_pub_;
ros::Publisher odom_gps_pub_;
ros::Publisher odom_fusion_pub_;
ros::Publisher robot_status_pub_;
ros::Publisher robot_switch_pub_;
ros::Publisher nav_status_pub_;

ros::Subscriber gps_fix_sub_;
ros::Subscriber joy_sub_;
ros::Subscriber imu_sub_;
ros::Subscriber odom_sub_;
ros::Subscriber gps_heading_sub_;
ros::Subscriber robot_status_sub_;

geometry_msgs::Twist cmd_vel_;
nav_msgs::Odometry odom_msg_;
nav_msgs::Odometry odom_fusion_;
sensor_msgs::Imu imu_msg_;
dbot_msgs::RobotStatus robot_status;
dbot_msgs::RobotSwitch robot_switch;
dbot_gps_nav::NavStatus nav_status;

geometry_msgs::PointStamped current_UTM_point;
geometry_msgs::PointStamped initial_UTM_point;
geometry_msgs::PointStamped target_UTM_point;
geometry_msgs::PointStamped initial_UTM_point_transformed;
geometry_msgs::PointStamped target_UTM_point_transformed;
geometry_msgs::PointStamped current_UTM_point_transformed;
geometry_msgs::PointStamped odom_point_old;
geometry_msgs::PointStamped odom_initial_point;
geometry_msgs::PointStamped current_odom_point_transformed;
std::vector<geometry_msgs::PointStamped*>* gps_waypoints;
std::vector<geometry_msgs::PointStamped*>* path_;

double heading_th = 0.0;
double heading_th_adjust = 0.0;
double heading_th_odom = 0.0;
double heading_th_imu = 0.0;
double heading_th_gps = 0.0;
double heading_th_gps_adjust = 0.0;
std::string utm_zone;

bool forward_flag = true;
bool emergency_method = true;
bool emergency_flag = false;
bool emergency_angle_flag = true;
bool emergency_return_flag = false;

sensor_msgs::Joy joy_;
ros::Time current_time, last_time;
ros::Time current_time_odom, last_time_odom;
ros::Duration duration_min(0.5);
int direction_change_delay;

double initial_lat;
double initial_lon;
double target_lat;
double target_lon;

double forward_increase_offset_on_start;
double forward_decrease_offset_on_reach;
double backward_increase_offset_on_start;
double backward_decrease_offset_on_reach;

double return_via_point;
double forward_decrease_speed_point;
double forward_increase_speed_point;
double backward_decrease_speed_point;
double backward_increase_speed_point;
double return_decrease_speed_point;

int boost_trigger_btn = 6;          //LT
int boost_mode_btn = 3;             //Y
int emergency_mode_btn = 3;         //Y
int boost_mode = 1;
int start_stop_following_btn = 1;   //A
int start_stop_following_flag = 0;
int save_trigger_btn = 7;           //RT
int action_trigger_btn = 5;         //RB
int change_direction_btn = 2;       //B
int save_path_file_btn = 0;         //X
int read_path_file_btn = 3;         //Y
int save_initial_point_btn = 1;     //A
int save_target_point_btn = 2;      //B

double x_bound = 0;
double distance_set = 0;
double distance_set_1;
double distance_set_2;
double distance_set_3;
double distance_set_4;
double safe_distance = 0.3;
double v_cmd = 0.0;
double v_tar = 0.0;
double v_tar_limit = 0.0;
double linear_speed_limit_boost;
double return_speed_limit;
double angular_speed_limit = M_PI / 4;

double offset_forward;
double offset_backward;

// back
// double ky = 1.0;
// double kth = 0.1;
// double k = 5.0;
double ky;
double k;
double k_fn;

double odom_x = 0;
double odom_y = 0;
double odom_th = 0;

double cur_th;
double path_th;

inline static double sqr(double x) {
    return x*x;
}

geometry_msgs::PointStamped latLongtoUTM(double lati_input, double longi_input)
{
    double utm_x = 0, utm_y = 0;
    geometry_msgs::PointStamped UTM_point_output;

    //convert lat/long to utm
    RobotLocalization::NavsatConversions::LLtoUTM(lati_input, longi_input, utm_y, utm_x, utm_zone);

    //Construct UTM_point and map_point geometry messages
    UTM_point_output.header.frame_id = "utm";
    UTM_point_output.header.stamp = ros::Time(0);
    UTM_point_output.point.x = utm_x;
    UTM_point_output.point.y = utm_y;
    UTM_point_output.point.z = 0;

    return UTM_point_output;
}

geometry_msgs::PointStamped rotatePoint(double x, double y, double th)
{
    geometry_msgs::PointStamped UTM_point_output;

    //Construct UTM_point and map_point geometry messages
    UTM_point_output.header.frame_id = "utm";
    UTM_point_output.header.stamp = ros::Time::now();
    UTM_point_output.point.x = cos(th)*x - sin(th)*y;
    UTM_point_output.point.y = sin(th)*x + cos(th)*y;
    UTM_point_output.point.z = 0;

    return UTM_point_output;
}

template <typename T>
void operator>>(const YAML::Node& node, T& i) {
  i = node.as<T>();
}

// parameter struct and typedef
struct WaypointNavParam {
  // waypoint filename
  std::string path_filename;
};

WaypointNavParam params_;
/// input image file name including the path
std::string path_filename_;

bool GetPathFromFile() {
  // clear way points vector
  gps_waypoints->clear();
  try {
    // check file open
    std::ifstream ifs(path_filename_.c_str(), std::ifstream::in);
    if (!ifs.good()) {
      ROS_WARN("!good");
      return false;
    }
    // yaml node
    YAML::Node yaml_node;
    yaml_node = YAML::Load(ifs);
    const YAML::Node& wp_node_tmp = yaml_node["waypoints"];
    const YAML::Node* wp_node = wp_node_tmp ? &wp_node_tmp : NULL;

    if (wp_node != NULL) {
      // loop over all the waypoints
      for (int i = 0; i < wp_node->size(); i++) {
        // get each waypoint
        geometry_msgs::PointStamped* path_point = new geometry_msgs::PointStamped();

        (*wp_node)[i]["point"]["x"] >> path_point->point.x;
        (*wp_node)[i]["point"]["y"] >> path_point->point.y;

        ROS_WARN("path received : %f, %f", path_point->point.x, path_point->point.y);

        gps_waypoints->push_back(path_point);
      }
    } else {
      ROS_WARN("else");
      return false;
    }
  } catch (YAML::ParserException& e) {
    ROS_WARN("ParserException");
    return false;
  } catch (YAML::RepresentationException& e) {
    ROS_WARN("RepresentationException");
    return false;
  }
  return true;
}

bool SavePath(){
  std::ofstream fout;
  fout.open(params_.path_filename.c_str());
  if (fout.is_open()){
    fout << std::fixed << std::setprecision(6);
    fout << initial_UTM_point.point.x << "," << initial_UTM_point.point.y << std::endl;
    fout << target_UTM_point.point.x << "," << target_UTM_point.point.y << std::endl;
    return true;
  } else {
    ROS_FATAL("Cannot save the path file");
    return false;
  }
}

void ParseColumns(const std::string& line, std::vector<std::string>* columns){
  std::istringstream ss(line);
  std::string column;

  while(std::getline(ss,column,',')){
    while(1){
      auto res = std::find(column.begin(), column.end(), ' ');
      if (res == column.end()){
        break;
      }
      column.erase(res);
    }
    if(!column.empty()){
      columns->emplace_back(column);
    }
  }
}

bool ReadPath(){
  std::string line;
  std::ifstream file(params_.path_filename);
  if(file.is_open()){
    bool first_columns = true;
    while(std::getline(file, line)){
      std::vector<std::string> columns;
      ParseColumns(line, &columns);
      target_UTM_point.point.x = std::stod(columns[0]);
      target_UTM_point.point.y = std::stod(columns[1]);
      if (first_columns){
        initial_UTM_point.point.x = std::stod(columns[0]);
        initial_UTM_point.point.y = std::stod(columns[1]);
      }
    }
    return true;
  } else {
    ROS_FATAL("Cannot open the path file");
    return false;
  }
}

double DistanceCalculatorFromCurrPose(double x, double y) {
  return sqrt(pow(current_UTM_point.point.x - x, 2) +  pow(current_UTM_point.point.y - y, 2) * 1.0); 
}

double DistanceCalculator(double x, double y, double cur_x, double cur_y) {
  return sqrt(pow(cur_x - x, 2) +  pow(cur_y - y, 2) * 1.0); 
}

double constrainAngle(double x){
    x = fmod(x + M_PI,M_PI*2);
    if (x < 0)
        x += M_PI*2;
    return x - M_PI;
}

void PointInitializer(geometry_msgs::PointStamped msg){
  msg.point.x = 0;
  msg.point.y = 0;
  msg.point.z = 0;
}

void CallBackGpsFix(const sensor_msgs::NavSatFix::ConstPtr& msg) {
  if(msg->position_covariance[0] < 0.001){
    nav_status.gps_error = false;
  } else nav_status.gps_error = true;
  current_UTM_point = latLongtoUTM(msg->latitude, msg->longitude);
}

void joyCallback(const sensor_msgs::Joy::ConstPtr& joy_msg){
  current_time = ros::Time::now();
  if (joy_msg->buttons[save_initial_point_btn] && joy_msg->buttons[save_trigger_btn] && (current_time - last_time > duration_min)){
    ROS_WARN("Initial point Saved!!!!!!!!!!!!!!!!!!!");
    ROS_WARN("x: %f, y: %f",current_UTM_point.point.x,current_UTM_point.point.y);
    initial_UTM_point.point.x = current_UTM_point.point.x;
    initial_UTM_point.point.y = current_UTM_point.point.y;
    last_time = current_time;
  } else if (joy_msg->buttons[save_target_point_btn] && joy_msg->buttons[save_trigger_btn] && (current_time - last_time > duration_min)){
    ROS_WARN("Target point Saved!!!!!!!!!!!!!!!!!!!");
    ROS_WARN("x: %f, y: %f",current_UTM_point.point.x,current_UTM_point.point.y);
    target_UTM_point.point.x = current_UTM_point.point.x;
    target_UTM_point.point.y = current_UTM_point.point.y;
    last_time = current_time;
  } else if(joy_msg->buttons[start_stop_following_btn] && joy_msg->buttons[action_trigger_btn] && start_stop_following_flag == 0 && (current_time - last_time > duration_min)){
    ROS_WARN("Start following!!!!!!!!!!!!!!!!!!");
    start_stop_following_flag = 1;
    robot_switch.start_stop_following_flag = start_stop_following_flag;
    robot_switch_pub_.publish(robot_switch);
    last_time = current_time;
  } else if (joy_msg->buttons[start_stop_following_btn] && joy_msg->buttons[action_trigger_btn] && start_stop_following_flag == 1 && (current_time - last_time > duration_min)){
    ROS_WARN("Stop following!!!!!!!!!!!!!!!!!!!");
    start_stop_following_flag = 0;
    cmd_vel_.angular.z = 0.0;
    cmd_vel_.linear.x = 0.0;
    v_tar = 0.0;
    cmd_pub_.publish(cmd_vel_);
    robot_switch.start_stop_following_flag = start_stop_following_flag;
    robot_switch_pub_.publish(robot_switch);
    last_time = current_time;
  } else if (joy_msg->buttons[change_direction_btn] && joy_msg->buttons[action_trigger_btn] && (current_time - last_time > duration_min)){
    ROS_WARN("Direction changed!!!!!!!!!!!!!!!!!!!");
    ROS_WARN("forward_flag     : %d", forward_flag);
    forward_flag = !forward_flag;
    last_time = current_time;
  } else if (joy_msg->buttons[emergency_mode_btn] && joy_msg->buttons[action_trigger_btn] && (current_time - last_time > duration_min)){
    if(emergency_flag){
      emergency_flag = false;
      start_stop_following_flag = false;
      emergency_angle_flag = true;
      emergency_return_flag = false;
    } else if(!emergency_flag){
      emergency_flag = true;
    }
    ROS_WARN("Emergency call pressed!!!!!!!!!!!!!!!!!!!");
    ROS_WARN("emergency_flag changed to : %d",emergency_flag);
    cmd_vel_.angular.z = 0.0;
    cmd_vel_.linear.x = 0.0;
    v_tar = 0.0;
    cmd_pub_.publish(cmd_vel_);
    last_time = current_time;
  } 
  // else if (joy_msg->buttons[save_path_file_btn] && joy_msg->buttons[save_trigger_btn] && (current_time - last_time > duration_min)){
  //   bool built = SavePath();
  //   if (!built) {
  //     ROS_FATAL("saving a file failed");
  //     return;
  //   } else {
  //     ROS_WARN("Waypoints saved in a text file");
  //   }
  //   last_time = current_time;
  // } else if (joy_msg->buttons[read_path_file_btn] && joy_msg->buttons[save_trigger_btn] && (current_time - last_time > duration_min)){
  //   bool built = ReadPath();
  //   if (!built) {
  //     ROS_FATAL("Loading a file failed");
  //     return;
  //   } else {
  //     ROS_WARN("Waypoints Loaded from a text file");
  //   }
  //   last_time = current_time;
  // } 
}

void statusCallback(const dbot_msgs::RobotStatus::ConstPtr& status_msg){
  robot_status.v_enc = status_msg->v_enc;
  robot_status.w_enc = status_msg->w_enc;
  nav_status.battery = status_msg->battery;
  current_time = ros::Time::now();
  if (status_msg->call_control == 49 && (current_time - last_time > duration_min)){
    distance_set = distance_set_1;
    ROS_WARN("distance_set changed to : %f",distance_set);
    last_time = current_time;
    nav_status.rf_remote = 1;
  } else if (status_msg->call_control == 50 && (current_time - last_time > duration_min)){
    distance_set = distance_set_2;
    ROS_WARN("distance_set changed to : %f",distance_set);
    last_time = current_time;
    nav_status.rf_remote = 2;
  } else if (status_msg->call_control == 51 && (current_time - last_time > duration_min)){
    distance_set = distance_set_3;
    ROS_WARN("distance_set changed to : %f",distance_set);
    last_time = current_time;
    nav_status.rf_remote = 3;
  } else if (status_msg->call_control == 52 && (current_time - last_time > duration_min)){
    distance_set = distance_set_4;
    ROS_WARN("distance_set changed to : %f",distance_set);
    last_time = current_time;
    nav_status.rf_remote = 4;
  } else if (status_msg->call_control == 53 && (current_time - last_time > duration_min)){
    ROS_WARN("Direction changed!!!!!!!!!!!!!!!!!!!");
    ROS_WARN("forward_flag     : %d", forward_flag);
    forward_flag = !forward_flag;
    last_time = current_time;
    nav_status.rf_remote = 5;
  } else if (status_msg->call_control == 58 && (current_time - last_time > duration_min)){
    if(start_stop_following_flag == 0){
      ROS_WARN("Start following!!!!!!!!!!!!!!!!!!!");
      start_stop_following_flag = 1;
      robot_switch.start_stop_following_flag = start_stop_following_flag;
      robot_switch_pub_.publish(robot_switch);
    }else if(start_stop_following_flag == 1){
      ROS_WARN("Stop following!!!!!!!!!!!!!!!!!!!");
      start_stop_following_flag = 0;
      cmd_vel_.angular.z = 0.0;
      cmd_vel_.linear.x = 0.0;
      v_tar = 0.0;
      cmd_pub_.publish(cmd_vel_);
      robot_switch.start_stop_following_flag = start_stop_following_flag;
      robot_switch_pub_.publish(robot_switch);
    }
    last_time = current_time;
    nav_status.rf_remote = 0;
  } else if (status_msg->call_control == 55 && (current_time - last_time > duration_min)){
    if(emergency_flag){
      emergency_flag = false;
      start_stop_following_flag = false;
      emergency_angle_flag = true;
      emergency_return_flag = false;
    } else if(!emergency_flag){
      emergency_flag = true;
      start_stop_following_flag = false;
      emergency_angle_flag = true;
      emergency_return_flag = false;
    }
    ROS_WARN("Emergency call pressed!!!!!!!!!!!!!!!!!!!");
    ROS_WARN("emergency_flag changed to : %d",emergency_flag);
    cmd_vel_.angular.z = 0.0;
    cmd_vel_.linear.x = 0.0;
    v_tar = 0.0;
    cmd_pub_.publish(cmd_vel_);
    last_time = current_time;
    nav_status.rf_remote = 7;
  } else if (status_msg->call_control == 56 && (current_time - last_time > duration_min)){
    ROS_WARN("Initial point Saved!!!!!!!!!!!!!!!!!!!");
    ROS_WARN("x: %f, y: %f",current_UTM_point.point.x,current_UTM_point.point.y);
    initial_UTM_point.point.x = current_UTM_point.point.x;
    initial_UTM_point.point.y = current_UTM_point.point.y;
    last_time = current_time;
    nav_status.rf_remote = 8;
  } else if (status_msg->call_control == 57 && (current_time - last_time > duration_min)){
    ROS_WARN("Target point Saved!!!!!!!!!!!!!!!!!!!");
    ROS_WARN("x: %f, y: %f",current_UTM_point.point.x,current_UTM_point.point.y);
    target_UTM_point.point.x = current_UTM_point.point.x;
    target_UTM_point.point.y = current_UTM_point.point.y;
    last_time = current_time;
    nav_status.rf_remote = 9;
  } 
}

void imuCallback(const sensor_msgs::Imu::ConstPtr& imu_msg){
  imu_msg_.linear_acceleration = imu_msg->linear_acceleration;
  imu_msg_.angular_velocity = imu_msg->angular_velocity;
  tf::Quaternion current_quaternion(
        imu_msg->orientation.x,
        imu_msg->orientation.y,
        imu_msg->orientation.z,
        imu_msg->orientation.w);
  tf::Matrix3x3 current_matrix(current_quaternion);
  double roll, pitch, yaw;
  current_matrix.getRPY(roll, pitch, yaw);
  heading_th_imu = constrainAngle(yaw - M_PI);
}

void odomCallback(const nav_msgs::Odometry::ConstPtr& msg){
  odom_msg_.pose = msg->pose;
  tf::Quaternion current_quaternion(
        msg->pose.pose.orientation.x,
        msg->pose.pose.orientation.y,
        msg->pose.pose.orientation.z,
        msg->pose.pose.orientation.w);
  tf::Matrix3x3 current_matrix(current_quaternion);
  double roll, pitch, yaw;
  double heading_th_tmp = heading_th_odom;
  current_matrix.getRPY(roll, pitch, yaw);
  heading_th_odom = yaw;
  if(heading_th_odom - heading_th_tmp < 0.5){
    heading_th = constrainAngle(heading_th + heading_th_odom - heading_th_tmp);
    heading_th_adjust = constrainAngle(-heading_th - M_PI/2);
    // ROS_WARN("Odom Yaw   : %f", heading_th_odom);
    // ROS_WARN("Fusion Yaw : %f", heading_th_adjust);
    // ROS_INFO("------------------------------------");

    geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(heading_th_adjust);
    nav_msgs::Odometry odom;
    odom.header.stamp = ros::Time::now();
    odom.header.frame_id = "odom";

    // set the position
    odom.pose.pose.position.x = current_UTM_point.point.x - initial_UTM_point.point.x;
    odom.pose.pose.position.y = current_UTM_point.point.y - initial_UTM_point.point.y;
    odom.pose.pose.position.z = 0.0;
    odom.pose.pose.orientation = odom_quat;

    // set the velocity
    // odom.child_frame_id = "base_link";
    // odom.twist.twist.linear.x = 0.0;
    // odom.twist.twist.linear.y = 0.0;
    // odom.twist.twist.angular.z = 0.0;

    // publish the message
    odom_fusion_pub_.publish(odom);
  }
}

void headingCallback(const geometry_msgs::QuaternionStamped::ConstPtr msg) {
  tf::Quaternion q(
      msg->quaternion.x,
      msg->quaternion.y,
      msg->quaternion.z,
      msg->quaternion.w);
  tf::Matrix3x3 m(q);
  double roll, pitch, yaw;
  m.getRPY(roll, pitch, yaw);
  int yaw_error = yaw * 1000000;
  if(yaw_error == 3141592) {
    nav_status.heading_error = true;
    ROS_FATAL("Heading error!!!");
  } else {
    nav_status.heading_error = false;
    // if(yaw_error != 3141592) std::cout << "heading angle : " << yaw << std::endl;
    heading_th_gps = constrainAngle(yaw);
    // ROS_INFO("heading_th      : %f", heading_th);
    // ROS_INFO("heading_th_gps  : %f", heading_th_gps);
    // ROS_INFO("abs(heading_th - heading_th_gps): %f", abs(heading_th - heading_th_gps));
    // if(abs(heading_th - heading_th_gps) < 1.57){
    heading_th = constrainAngle((heading_th + heading_th_gps)/2);
    heading_th_gps_adjust = constrainAngle(-heading_th_gps - M_PI/2);
    // ROS_INFO("GPS Yaw    : %f", heading_th_gps_adjust);

    geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(heading_th_gps_adjust);
    nav_msgs::Odometry odom;
    odom.header.stamp = ros::Time::now();
    odom.header.frame_id = "odom";

    // set the position
    odom.pose.pose.position.x = current_UTM_point.point.x - initial_UTM_point.point.x;
    odom.pose.pose.position.y = current_UTM_point.point.y - initial_UTM_point.point.y;
    odom.pose.pose.position.z = 0.0;
    odom.pose.pose.orientation = odom_quat;

    // set the velocity
    // odom.child_frame_id = "base_link";
    // odom.twist.twist.linear.x = 0.0;
    // odom.twist.twist.linear.y = 0.0;
    // odom.twist.twist.angular.z = 0.0;

    // publish the message
    odom_gps_pub_.publish(odom);
    // }
  }
}

void gpsControl(){
  current_time_odom = ros::Time::now();

  double vx = robot_status.v_enc;
  double vy = 0;
  double vth = imu_msg_.angular_velocity.z;

  if(abs(vth) < 0.03 && vx == 0.0) vth = 0.0;

  // compute odometry in a typical way given the velocities of the robot
  double dt = (current_time_odom - last_time_odom).toSec();
  double delta_x = (vx * cos(constrainAngle(heading_th_adjust))) * dt;
  double delta_y = (vx * sin(constrainAngle(heading_th_adjust))) * dt;
  double delta_th = vth * dt;

  // current_UTM_point.point.x += delta_x;
  // current_UTM_point.point.y += delta_y;
  // odom_th += delta_th;

  last_time_odom = current_time_odom;

  double distance_target = DistanceCalculatorFromCurrPose(target_UTM_point.point.x, target_UTM_point.point.y);
  double distance_initial = DistanceCalculatorFromCurrPose(initial_UTM_point.point.x, initial_UTM_point.point.y);
  double dff_path_x = target_UTM_point.point.x - initial_UTM_point.point.x;
  double dff_path_y = target_UTM_point.point.y - initial_UTM_point.point.y;
  path_th = atan2(dff_path_y, dff_path_x);

  double diff_x = current_UTM_point.point.x - initial_UTM_point.point.x;
  double diff_y = current_UTM_point.point.y - initial_UTM_point.point.y;

  current_UTM_point_transformed = rotatePoint(diff_x, diff_y, -path_th);
  target_UTM_point_transformed = rotatePoint(dff_path_x, dff_path_y, -path_th);

  x_bound = current_UTM_point_transformed.point.x;

  // odometry control part
  // path_th = 0.0;
  // heading_th_adjust = heading_th_odom;
  // current_UTM_point_transformed.point.x = odom_msg_.pose.pose.position.x;
  // current_UTM_point_transformed.point.y = odom_msg_.pose.pose.position.y;
  // target_UTM_point_transformed.point.x = 10.0;
  // target_UTM_point_transformed.point.y = 0.0;
  // distance_target = DistanceCalculator(target_UTM_point_transformed.point.x, target_UTM_point_transformed.point.y, current_UTM_point_transformed.point.x, current_UTM_point_transformed.point.y);
  // distance_initial = DistanceCalculator(0, 0, current_UTM_point_transformed.point.x, current_UTM_point_transformed.point.y);
  // std::cout << "current_UTM_point_transformed.point.x      : " << current_UTM_point_transformed.point.x << std::endl;
  // std::cout << "current_UTM_point_transformed.point.y      : " << current_UTM_point_transformed.point.y << std::endl;

  if(distance_set != 0 && forward_flag){
    distance_target = DistanceCalculator(distance_set, 0, current_UTM_point_transformed.point.x, current_UTM_point_transformed.point.y);
    if(current_UTM_point_transformed.point.x > distance_set){
      ROS_WARN("Direction Changed to backward at %f!!!!!!!", distance_set);
      forward_flag = false;
      cmd_vel_.angular.z = 0.0;
      cmd_vel_.linear.x = 0.0;
      v_tar = 0.0;
      cmd_pub_.publish(cmd_vel_);
      sleep(direction_change_delay);
    } 
  }else if(current_UTM_point_transformed.point.x > target_UTM_point_transformed.point.x && forward_flag){
    ROS_WARN("Direction Changed to backward!!!!!!!!!!!");
    forward_flag = !forward_flag;
    cmd_vel_.angular.z = 0.0;
    cmd_vel_.linear.x = 0.0;
    v_tar = 0.0;
    cmd_pub_.publish(cmd_vel_);
    sleep(direction_change_delay);
  } else if(current_UTM_point_transformed.point.x < 0.0 && !forward_flag){
    ROS_WARN("Direction Changed to forward!!!!!!!!!!!!");
    forward_flag = !forward_flag;
    cmd_vel_.angular.z = 0.0;
    cmd_vel_.linear.x = 0.0;
    v_tar = 0.0;
    cmd_pub_.publish(cmd_vel_);
    if(emergency_return_flag) {
      start_stop_following_flag = false;
      emergency_return_flag = false;
    }
    sleep(direction_change_delay);
  } 
  if(start_stop_following_flag){
    double final_th;
    double desired_th;
    double offset_scale = 1.0;
    if(forward_flag) {
      if(distance_initial < forward_increase_offset_on_start) {
        offset_scale = distance_initial / forward_increase_offset_on_start;
      }
      if(distance_target < forward_decrease_offset_on_reach) {
        offset_scale = distance_target / forward_decrease_offset_on_reach;
      }
    } else if(!forward_flag) {
      if(distance_target < backward_increase_offset_on_start) {
        offset_scale = distance_target / backward_increase_offset_on_start;
      }
      if(distance_initial < backward_increase_offset_on_start) {
        offset_scale = distance_initial / backward_increase_offset_on_start;
      }
    }
    //if(distance_target < 10 || distance_initial < 10) offset_scale = 0.5;
    if(forward_flag) desired_th = -atan(ky*(current_UTM_point_transformed.point.y - offset_forward * offset_scale))/k_fn;
    if(!forward_flag) desired_th = -atan(ky*(current_UTM_point_transformed.point.y - offset_backward * offset_scale))/k_fn;
    final_th = heading_th_adjust - path_th;

    double linear_speed_control_th;
    if(forward_flag){
      linear_speed_control_th = desired_th - final_th;
    } else if(!forward_flag){
      linear_speed_control_th = - desired_th - final_th;
    }
    // double final_linear_speed = linear_speed_limit * cos(M_PI / 2 * tanh(k * (final_th)));
    double final_linear_speed_boost = linear_speed_limit_boost * cos(M_PI / 2 * tanh(k * (final_th)));
    if(distance_target > safe_distance && forward_flag){
      // if(boost_mode == 0) v_tar_limit = final_linear_speed;
      if(boost_mode == 1) v_tar_limit = final_linear_speed_boost;
    } else if(distance_initial > safe_distance && !forward_flag){
      // if(boost_mode == 0) v_tar_limit = - final_linear_speed;
      if(boost_mode == 1) v_tar_limit = - final_linear_speed_boost;
    }

    if(forward_flag) {
      cmd_vel_.angular.z = (desired_th - final_th*3.5) * 0.1;
      v_tar = v_tar_limit;
      if(distance_target < forward_decrease_speed_point){
        v_tar = v_tar_limit * (distance_target / forward_decrease_speed_point);
        if(v_tar < 0.2) v_tar = 0.2;
        // ROS_WARN("Reaching target point, slowing down...  %f", v_tar);
      }
      if(distance_initial < forward_increase_speed_point){
        v_tar = v_tar_limit * (distance_initial / forward_increase_speed_point);
        if(v_tar < 0.3) v_tar = 0.3;
      }
      cmd_vel_.linear.x = v_tar;
      // std::cout << "---------------------------angle adjusted---------------------------" << std::endl;
    } else if(!forward_flag) {
      cmd_vel_.angular.z = (- desired_th - final_th*3.5) * 0.04;
      v_tar = v_tar_limit;
      if(distance_initial < backward_decrease_speed_point){
        v_tar = v_tar_limit * (distance_initial / backward_decrease_speed_point);
        if(v_tar > -0.2) v_tar = -0.2;
        // ROS_WARN("Reaching initial point, slowing down...  %f", v_tar);
      }
      if(distance_target < backward_increase_speed_point){
        v_tar = v_tar_limit * (distance_target / backward_increase_speed_point);
        if(v_tar > -0.3) v_tar = -0.3;
      }
      cmd_vel_.linear.x = v_tar;
      // std::cout << "---------------------------angle adjusted---------------------------" << std::endl;
    }

    if(abs(cmd_vel_.angular.z) > M_PI/4 && abs(current_UTM_point_transformed.point.y) < 0.3) ROS_FATAL("Too much angle difference!!!");
    if(cmd_vel_.angular.z > angular_speed_limit) cmd_vel_.angular.z = angular_speed_limit;
    if(cmd_vel_.angular.z < - angular_speed_limit) cmd_vel_.angular.z = - angular_speed_limit;
    
    std::cout << current_UTM_point_transformed.point.x << "   " << current_UTM_point_transformed.point.y << "   " << desired_th << "   " << final_th << "   " << cmd_vel_.linear.x << "   " << cmd_vel_.angular.z << "   " << robot_status.v_enc << "   " << robot_status.w_enc << "   " << imu_msg_.linear_acceleration.x << "   " << imu_msg_.angular_velocity.z << std::endl;

    nav_status.current_x = current_UTM_point_transformed.point.x;
    nav_status.current_y = current_UTM_point_transformed.point.y;

    cmd_pub_.publish(cmd_vel_);
    return;
  }
}

void emergencyControl(){
  double dff_path_x = target_UTM_point.point.x - initial_UTM_point.point.x;
  double dff_path_y = target_UTM_point.point.y - initial_UTM_point.point.y;
  path_th = atan2(dff_path_y, dff_path_x);

  double diff_x = current_UTM_point.point.x - initial_UTM_point.point.x;
  double diff_y = current_UTM_point.point.y - initial_UTM_point.point.y;

  current_UTM_point_transformed = rotatePoint(diff_x, diff_y, -path_th);
  target_UTM_point_transformed = rotatePoint(dff_path_x, dff_path_y, -path_th);

  double distance_via_point = DistanceCalculator(return_via_point, 0, current_UTM_point_transformed.point.x, current_UTM_point_transformed.point.y);

  x_bound = current_UTM_point_transformed.point.x - return_via_point;

  double dff_path_x_tr = current_UTM_point_transformed.point.x - return_via_point;
  double dff_path_y_tr = current_UTM_point_transformed.point.y;
  double emergency_th = (atan2(dff_path_y_tr, dff_path_x_tr));
  double emergency_final_th = (emergency_th - (heading_th_adjust - path_th));
  constrainAngle(emergency_final_th);
  std::cout << "current.point.x             : " << current_UTM_point_transformed.point.x << std::endl;
  std::cout << "current.point.y             : " << current_UTM_point_transformed.point.y << std::endl;
  std::cout << "x_bound                     : " << x_bound << std::endl;
  std::cout << "emergency_th                : " << emergency_th << std::endl;
  std::cout << "heading_th_adjust - path_th : " << heading_th_adjust - path_th << std::endl;
  std::cout << "emergency_final_th          : " << emergency_final_th << std::endl;

  if(abs(x_bound) < 0.1 && emergency_angle_flag){
    ROS_WARN("Reached at starting point!!!!!!!!!!!!");
    ROS_WARN("Matching direction......");
    forward_flag = false;
    emergency_angle_flag = false;
    cmd_vel_.angular.z = 0.0;
    cmd_vel_.linear.x = 0.0;
    cmd_pub_.publish(cmd_vel_);
    sleep(2);
  } 
  if(abs(heading_th_adjust - path_th) < 0.05 && !emergency_angle_flag){
    ROS_WARN("Direction matched at starting point!!!!!!!!!!!!");
    emergency_angle_flag = true;
    emergency_flag = false;
    emergency_return_flag = true;
    cmd_vel_.angular.z = 0.0;
    cmd_vel_.linear.x = 0.0;
    cmd_pub_.publish(cmd_vel_);
    sleep(2);
    start_stop_following_flag = true;
  }
  if(!emergency_angle_flag){
    if(constrainAngle(heading_th_adjust - path_th) > 0){
      cmd_vel_.linear.x = 0.0;
      cmd_vel_.angular.z = - 0.06;
      cmd_pub_.publish(cmd_vel_);
    } else {
      cmd_vel_.linear.x = 0.0;
      cmd_vel_.angular.z = 0.06;
      cmd_pub_.publish(cmd_vel_);
    }
  } 
  if(emergency_flag && emergency_angle_flag && emergency_method){
    if(x_bound < 0){
      forward_flag = true;
    } else {
      forward_flag = false;
    }
    if(forward_flag) {
      cmd_vel_.linear.x = return_speed_limit;
      if(distance_via_point < return_decrease_speed_point){
        cmd_vel_.linear.x = return_speed_limit * (distance_via_point / return_decrease_speed_point);
        if(cmd_vel_.linear.x < 0.2) cmd_vel_.linear.x = 0.2;
      }
      cmd_vel_.angular.z = constrainAngle(emergency_final_th + M_PI) * 0.5;
    } else if(!forward_flag){
      cmd_vel_.linear.x = - return_speed_limit;
      if(distance_via_point < return_decrease_speed_point){
        cmd_vel_.linear.x = - return_speed_limit * (distance_via_point / return_decrease_speed_point);
        if(abs(cmd_vel_.linear.x) < 0.2) cmd_vel_.linear.x = -0.2;
      }
      cmd_vel_.angular.z = emergency_final_th * 0.5;
    }
    std::cout << "---------------------------angle adjusted---------------------------" << std::endl;
    std::cout << "---------------------emergency direct callback----------------------" << std::endl;
    if(cmd_vel_.angular.z > angular_speed_limit) cmd_vel_.angular.z = angular_speed_limit;
    if(cmd_vel_.angular.z < - angular_speed_limit) cmd_vel_.angular.z = - angular_speed_limit;
    cmd_pub_.publish(cmd_vel_);
    std::cout << "--------------------------------------------------------------------" << std::endl;
    return;
  } 
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "dbot_gps_nav_node");
  ros::NodeHandle nh = ros::NodeHandle(), private_nh = ros::NodeHandle("~");

  PointInitializer(current_UTM_point);
  PointInitializer(current_UTM_point_transformed);

  std::string param_ns_prefix_ = "/dbot_gps_nav";

  nh.getParam(param_ns_prefix_ + "/distance_set_1", distance_set_1);
  nh.getParam(param_ns_prefix_ + "/distance_set_2", distance_set_2);
  nh.getParam(param_ns_prefix_ + "/distance_set_3", distance_set_3);
  nh.getParam(param_ns_prefix_ + "/distance_set_4", distance_set_4);
  nh.getParam(param_ns_prefix_ + "/offset_forward", offset_forward);
  nh.getParam(param_ns_prefix_ + "/offset_backward", offset_backward);
  nh.getParam(param_ns_prefix_ + "/direction_change_delay", direction_change_delay);
  nh.getParam(param_ns_prefix_ + "/linear_speed_limit_boost", linear_speed_limit_boost);
  nh.getParam(param_ns_prefix_ + "/return_speed_limit", return_speed_limit);
  nh.getParam(param_ns_prefix_ + "/ky", ky);
  nh.getParam(param_ns_prefix_ + "/k", k);
  nh.getParam(param_ns_prefix_ + "/k_fn", k_fn);
  nh.getParam(param_ns_prefix_ + "/forward_increase_offset_on_start", forward_increase_offset_on_start);
  nh.getParam(param_ns_prefix_ + "/forward_decrease_offset_on_reach", forward_decrease_offset_on_reach);
  nh.getParam(param_ns_prefix_ + "/backward_increase_offset_on_start", backward_increase_offset_on_start);
  nh.getParam(param_ns_prefix_ + "/backward_decrease_offset_on_reach", backward_decrease_offset_on_reach);
  nh.getParam(param_ns_prefix_ + "/return_via_point", return_via_point);
  nh.getParam(param_ns_prefix_ + "/forward_decrease_speed_point", forward_decrease_speed_point);
  nh.getParam(param_ns_prefix_ + "/forward_increase_speed_point", forward_increase_speed_point);
  nh.getParam(param_ns_prefix_ + "/backward_decrease_speed_point", backward_decrease_speed_point);
  nh.getParam(param_ns_prefix_ + "/backward_increase_speed_point", backward_increase_speed_point);
  nh.getParam(param_ns_prefix_ + "/return_decrease_speed_point", return_decrease_speed_point);
  nh.getParam(param_ns_prefix_ + "/initial_lat", initial_lat);
  nh.getParam(param_ns_prefix_ + "/initial_lon", initial_lon);
  nh.getParam(param_ns_prefix_ + "/target_lat", target_lat);
  nh.getParam(param_ns_prefix_ + "/target_lon", target_lon);

  initial_UTM_point = latLongtoUTM(initial_lat, initial_lon);
  target_UTM_point = latLongtoUTM(target_lat, target_lon);
  // initial_UTM_point = latLongtoUTM(37.5636387833, 126.892841713);       // Peace Park
  // target_UTM_point = latLongtoUTM(37.5621542033, 126.891489443);//Torino Mark
  // initial_UTM_point = latLongtoUTM(37.5622140167, 126.934073628);       // Track
  // target_UTM_point = latLongtoUTM(37.5617708383, 126.933417688);
  // initial_UTM_point = latLongtoUTM(37.5619083117, 126.933973427);       // Track side
  // target_UTM_point = latLongtoUTM(37.5616254217, 126.933512672);
  // initial_UTM_point = latLongtoUTM(37.561075725, 126.936530813);       // Sideroad
  // target_UTM_point = latLongtoUTM(37.5610112483, 126.936891832);
  // initial_UTM_point = latLongtoUTM(37.56063997, 126.936848677);        // Baekyang(Gate ~ Fountain)
  // target_UTM_point = latLongtoUTM(37.5623092817, 126.937393107);
  // initial_UTM_point = latLongtoUTM(37.56063997, 126.936848677);        // Baekyang(Gate ~ Engineering Bldg A)
  // target_UTM_point = latLongtoUTM(37.5616159533, 126.937166922);
  // initial_UTM_point = latLongtoUTM(37.5607819217, 126.936250158);       // Frontyard
  // target_UTM_point = latLongtoUTM(37.5606736417, 126.936213908);
  // initial_UTM_point = latLongtoUTM(37.5607816533, 126.936230398);       // Frontyard Long
  // target_UTM_point = latLongtoUTM(37.560541915, 126.93615099);
  // geometry_msgs::PointStamped initial_UTM_point_old = latLongtoUTM(36.7180531367, 126.360806198);       // Seosan test field(Center)
  // geometry_msgs::PointStamped target_UTM_point_old = latLongtoUTM(36.7153610967, 126.361109545);        // 300m
  // initial_UTM_point = latLongtoUTM(36.7180531367, 126.360806198);     // Seosan test field(Center) 210421
  // target_UTM_point = latLongtoUTM(36.7153610967, 126.361109545);       // 300m        // 300m
  // initial_UTM_point = latLongtoUTM(36.7180531483, 126.360806365);       // Seosan test field(Center) 210421
  // target_UTM_point = latLongtoUTM(36.715361235, 126.36110963);        // 300m
  // initial_UTM_point = latLongtoUTM(36.7180268383, 126.36042849);       // Seosan test field(Side) 210506
  // target_UTM_point = latLongtoUTM(36.7159182083, 126.360666145);        // 300m
  // initial_UTM_point = latLongtoUTM(36.7180913233, 126.361325958);       // Seosan test field(Side) 210506
  // target_UTM_point = latLongtoUTM(36.7159056233, 126.361572293);        // 240m
  // initial_UTM_point = latLongtoUTM(36.7180224767, 126.360348197);       // Seosan test field(Side center) 210507
  // target_UTM_point = latLongtoUTM(36.7179042833, 126.360360652);        // 15m
  // initial_UTM_point = latLongtoUTM(36.7180226067, 126.360347228);       // Seosan test field(Side center) 210507
  // target_UTM_point = latLongtoUTM(36.7153272983, 126.360650708);        // 300m

  target_UTM_point_transformed = rotatePoint(target_UTM_point.point.x - initial_UTM_point.point.x, target_UTM_point.point.y - initial_UTM_point.point.y, -path_th);

  // std::cout << "Initial point diff : " << DistanceCalculator(initial_UTM_point_old.point.x, initial_UTM_point_old.point.y, initial_UTM_point.point.x, initial_UTM_point.point.y) << std::endl;
  // std::cout << "Target point diff  : " << DistanceCalculator(target_UTM_point_old.point.x, target_UTM_point_old.point.y, target_UTM_point.point.x, target_UTM_point.point.y) << std::endl;

  joy_sub_ = nh.subscribe("/joy", 1, joyCallback);
  imu_sub_ = nh.subscribe("/imu/data", 1, imuCallback);
  odom_sub_ = nh.subscribe("/dogu/odom", 1, odomCallback);
  robot_status_sub_ = nh.subscribe("/robot_status", 1, statusCallback);
  gps_fix_sub_ = nh.subscribe("/fix", 1, CallBackGpsFix);
  gps_heading_sub_ = nh.subscribe("/heading", 1, headingCallback);

  cmd_pub_ = nh.advertise<geometry_msgs::Twist>("/cmd_vel", 1);
  robot_switch_pub_ = nh.advertise<dbot_msgs::RobotSwitch>("/robot_switch", 1);
  odom_gps_pub_ = nh.advertise<nav_msgs::Odometry>("/odom/gps", 1);
  odom_fusion_pub_ = nh.advertise<nav_msgs::Odometry>("/odom/fusion", 1);
  nav_status_pub_ = nh.advertise<dbot_gps_nav::NavStatus>("/nav_status", 1);

  gps_waypoints = new std::vector<geometry_msgs::PointStamped*>();

  nh.param<std::string>("path_filename", params_.path_filename,
                        "/home/dogu/dogu_system/path/path.txt");
  std::string pkg_path = ros::package::getPath("dbot_gps_nav");
  path_filename_ = pkg_path + "/" + params_.path_filename;
  path_ = new std::vector<geometry_msgs::PointStamped*>();

  ros::Rate r(30.0);
  while (nh.ok()) {
    ros::spinOnce();  // check for incoming messages
    if(!emergency_flag) gpsControl();
    if(emergency_flag) emergencyControl();
    nav_status_pub_.publish(nav_status);
    r.sleep();
  }

  return 0;
}