# CMake generated Testfile for 
# Source directory: /home/dogu/catkin_ws/src/twist_mux
# Build directory: /home/dogu/catkin_ws/src/twist_mux/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_twist_mux_rostest_test_system.test "/home/dogu/catkin_ws/src/twist_mux/build/catkin_generated/env_cached.sh" "/usr/bin/python2" "/opt/ros/melodic/share/catkin/cmake/test/run_tests.py" "/home/dogu/catkin_ws/src/twist_mux/build/test_results/twist_mux/rostest-test_system.xml" "--return-code" "/opt/ros/melodic/share/rostest/cmake/../../../bin/rostest --pkgdir=/home/dogu/catkin_ws/src/twist_mux --package=twist_mux --results-filename test_system.xml --results-base-dir \"/home/dogu/catkin_ws/src/twist_mux/build/test_results\" /home/dogu/catkin_ws/src/twist_mux/test/system.test ")
subdirs("gtest")
