#include <cmath>
#include <geometry_msgs/Transform.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseStamped.h>
#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <sensor_msgs/NavSatFix.h>
#include <nav_msgs/Odometry.h>
#include <robot_localization/navsat_conversions.h>
#include <geometry_msgs/PointStamped.h>
#include <tf/transform_listener.h>

ros::Publisher cmd_pub_;

ros::Subscriber current_pose_sub_;
ros::Subscriber gps_fix_sub_;
ros::Subscriber joy_sub_;
ros::Subscriber odom_sub_;

geometry_msgs::PointStamped UTM_point, map_point, UTM_next, map_next;

// uint8_t driving_mode_ = 0;

geometry_msgs::Pose current_pose_;
geometry_msgs::Twist cmd_vel_;

// sensor_msgs::NavSatFix waypoint_;
// geometry_msgs::PointStamped current_UTM_point;
// geometry_msgs::PointStamped old_UTM_point;
// geometry_msgs::PointStamped target_UTM_point;
// geometry_msgs::PointStamped initial_UTM_point;
// geometry_msgs::PointStamped set_heading_start_point;
// geometry_msgs::PointStamped set_heading_end_point;
std::string utm_zone;

nav_msgs::Odometry current_odom;
nav_msgs::Odometry target_odom;

double roll, pitch, yaw;

bool forward_flag = true;

sensor_msgs::Joy joy_;
ros::Time current_time, last_time;
ros::Duration duration_min(1);
ros::Duration ten_seconds(10);

int start_stop_following_btn = 1;   //A
int start_stop_following_flag = 1;
int follow_next_waypoint_btn = 2;   //B
int follow_next_waypoint_flag = 0;
int set_heading_btn = 3;            //Y
int set_heading_flag = 0;
int backward_flag = 0;

uint waypoint_id = 0;

double target_goal = 5.0;
double origin_goal = 0.0;

double distance_current = 0;
double distance_current_from_old = 0;
double distance_next = 0;
double distance_limit = 1.5; // meter scale
double safe_distance = 0.3;
double linear_speed_limit = 0.5;

double DistanceCalculatorFromCurrPose(double x, double y) {
  // ROS_WARN("curr_UTM_x : %f, curr_UTM_y : %f", current_UTM_point.point.x, current_UTM_point.point.y);
  // ROS_WARN("tar_UTM_x : %f, tar_UTM_y : %f", x, y);
  return sqrt(pow(current_pose_.position.x - x, 2) +  pow(current_pose_.position.y - y, 2) * 1.0); 
}

double constrainAngle(double x){
    x = fmod(x + M_PI,M_PI*2);
    if (x < 0)
        x += M_PI*2;
    return x - M_PI;
}

// double linear_vel(self, goal_pose, constant=1.5){
//   return constant * self.euclidean_distance(goal_pose);
// }

double SteeringAngle(){
  return atan2(target_odom.pose.pose.position.y - current_pose_.position.y, target_odom.pose.pose.position.x - current_pose_.position.x);
}
double AngularVel(){
  double constant = 2;
  return constant * constrainAngle(SteeringAngle() - yaw);
}
double AngularVelBackward(){
  double constant = 2;
  return constant * constrainAngle(SteeringAngle() - yaw + M_PI);
}

void odomCallback(const nav_msgs::Odometry::ConstPtr& msg) {
  current_odom.pose = msg->pose;
}

void currentPoseCallback(const geometry_msgs::Pose& msg){
  current_pose_ = msg;

  tf::Quaternion current_q(current_pose_.orientation.x,
      current_pose_.orientation.y,
      current_pose_.orientation.z,
      current_pose_.orientation.w);
  tf::Matrix3x3 m(current_q);
  m.getRPY(roll, pitch, yaw); 
}

void joyCallback(const sensor_msgs::Joy::ConstPtr& joy_msg){
  current_time = ros::Time::now();
  if(joy_msg->buttons[start_stop_following_btn] && start_stop_following_btn ==0 && (current_time - last_time > duration_min)){
    ROS_WARN("Start following!!!!!!!!!!!!!!!!!!!");
    start_stop_following_flag = 1;
    last_time = current_time;
  } else if (joy_msg->buttons[start_stop_following_btn] && start_stop_following_btn ==1&& (current_time - last_time > duration_min)){
    ROS_WARN("Stop following!!!!!!!!!!!!!!!!!!!");
    start_stop_following_flag = 0;
    last_time = current_time;
  }
}

geometry_msgs::PointStamped latLongtoUTM(double lati_input, double longi_input)
{
    double utm_x = 0, utm_y = 0;
    geometry_msgs::PointStamped UTM_point_output;

    //convert lat/long to utm
    RobotLocalization::NavsatConversions::LLtoUTM(lati_input, longi_input, utm_y, utm_x, utm_zone);

    //Construct UTM_point and map_point geometry messages
    UTM_point_output.header.frame_id = "utm";
    UTM_point_output.header.stamp = ros::Time(0);
    UTM_point_output.point.x = utm_x;
    UTM_point_output.point.y = utm_y;
    UTM_point_output.point.z = 0;

    return UTM_point_output;
}

geometry_msgs::PointStamped UTMtoMapPoint(geometry_msgs::PointStamped UTM_input)
{
    geometry_msgs::PointStamped map_point_output;
    bool notDone = true;
    tf::TransformListener listener; //create transformlistener object called listener
    ros::Time time_now = ros::Time::now();
    while(notDone)
    {
        try
        {
            UTM_point.header.stamp = ros::Time::now();
            listener.waitForTransform("odom", "utm", time_now, ros::Duration(3.0));
            listener.transformPoint("odom", UTM_input, map_point_output);
            notDone = false;
        }
        catch (tf::TransformException& ex)
        {
            ROS_WARN("%s", ex.what());
            ros::Duration(0.01).sleep();
            //return;
        }
    }
    return map_point_output;
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "control_node");
  ros::NodeHandle nh = ros::NodeHandle(), private_nh = ros::NodeHandle("~");

  odom_sub_ = nh.subscribe("/dogu/odom", 1, odomCallback);
  joy_sub_ = nh.subscribe("/joy", 1, joyCallback);
  current_pose_sub_ = nh.subscribe("/robot_pose", 1, currentPoseCallback);

  cmd_pub_ = nh.advertise<geometry_msgs::Twist>("/cmd_vel", 1);


  //Convert lat/long to utm:
  // UTM_point = latLongtoUTM(36.7182346317, 126.360562927);
  // UTM_next = latLongtoUTM(36.7182426817,126.360674645);
  //       //Transform UTM to map point in odom frame
  // map_point = UTMtoMapPoint(UTM_point);
  // map_next = UTMtoMapPoint(UTM_next);
  target_odom.pose.pose.position.x = -2.0;//map_point.point.x;
  target_odom.pose.pose.position.y = 0.0;//map_point.point.y;


  ros::Rate r(20.0);
  while(nh.ok()){
    distance_current = DistanceCalculatorFromCurrPose(target_odom.pose.pose.position.x, target_odom.pose.pose.position.y);
    ROS_WARN("steering_angle  : %f", SteeringAngle());
    ROS_WARN("yaw             : %f", yaw);
    
    if(!backward_flag){
      if(distance_current >= 0.01 && start_stop_following_flag){
        target_odom.pose.pose.position.x = -2.0;//map_point.point.x;
        target_odom.pose.pose.position.y = 0.0;//map_point.point.y;
        cmd_vel_.linear.x = distance_current;
        if(cmd_vel_.linear.x > 0.5) cmd_vel_.linear.x = 0.5;
        cmd_vel_.linear.y = 0.0;
        cmd_vel_.linear.z = 0.0;

        cmd_vel_.angular.x = 0.0;
        cmd_vel_.angular.y = 0.0;
        cmd_vel_.angular.z = AngularVel();
        ROS_INFO("linear: %f, angular: %f, distance: %f", cmd_vel_.linear.x, cmd_vel_.angular.z, distance_current);
        cmd_pub_.publish(cmd_vel_);
        if(DistanceCalculatorFromCurrPose(target_odom.pose.pose.position.x, target_odom.pose.pose.position.y) <= 0.2) {
          target_odom.pose.pose.position.x = 0.0;//map_next.point.x;
          target_odom.pose.pose.position.y = 0.0;//map_next.point.y;
          cmd_vel_.linear.x = 0.0;
          cmd_vel_.angular.z = 0.0;
          cmd_pub_.publish(cmd_vel_);
          backward_flag = 1;
          ROS_INFO("Reached goal, sending back!!!");
        }
      }
    } else if(backward_flag){
      if(distance_current >= 0.01 && start_stop_following_flag){
        cmd_vel_.linear.x = -distance_current;
        if(abs(cmd_vel_.linear.x) > 0.5) cmd_vel_.linear.x = -0.5;
        cmd_vel_.linear.y = 0.0;
        cmd_vel_.linear.z = 0.0;

        cmd_vel_.angular.x = 0.0;
        cmd_vel_.angular.y = 0.0;
        cmd_vel_.angular.z = AngularVelBackward();
        ROS_INFO("linear: %f, angular: %f, distance: %f", cmd_vel_.linear.x, cmd_vel_.angular.z, distance_current);
        cmd_pub_.publish(cmd_vel_);
        if(DistanceCalculatorFromCurrPose(target_odom.pose.pose.position.x, target_odom.pose.pose.position.y) <= 0.2) {
          // start_stop_following_flag = 0;
          target_odom.pose.pose.position.x = -2.0;//map_point.point.x;
          target_odom.pose.pose.position.y = 0.0;//map_point.point.y;
          cmd_vel_.linear.x = 0.0;
          cmd_vel_.angular.z = 0.0;
          cmd_pub_.publish(cmd_vel_);
          backward_flag = 0;
          ROS_INFO("Reached origin, finishing!!!");
        }
      }
    }
    ros::spinOnce();  // check for incoming messages
    r.sleep();
  }

  // ros::MultiThreadedSpinner spinner(4);
  // spinner.spin();

  // return 0;
}