#!/bin/bash

echo "Delete remap device ports"

sudo rm /etc/udev/rules.d/dbot.rules  

echo "Restarting udevadm"

udevadm control --reload-rules && udevadm trigger

echo "Finish"