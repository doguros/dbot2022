#!/bin/bash
echo "Remap device ports"

sudo cp `rospack find dbot_bringup`/scripts/udev/dbot.rules /etc/udev/rules.d/dbot.rules

echo "Restarting udevadm"

udevadm control --reload-rules && udevadm trigger

echo "Finish"
