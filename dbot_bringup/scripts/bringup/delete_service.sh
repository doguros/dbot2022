#!/bin/bash
echo " "
echo "Start to remove script files from /home/"
echo ""
rm  /home/dbot-start.sh
echo " "
echo "Start to remove service files from /lib/systemd/system/"
echo ""
sudo rm  /lib/systemd/system/dbot.service
echo " "
echo "Disable max-performance and roborts service for upstart! "
echo ""
sudo systemctl disable dbot.service
echo "Finish"