#!/bin/bash

echo " "
echo "Start to copy script to /home/"
echo " "

sudo cp `rospack find dbot_bringup`/scripts/bringup/dbot-start.sh /home/

echo " "
echo "Start to copy service files to /lib/systemd/system"
echo " "

sudo cp `rospack find dbot_bringup`/scripts/bringup/dbot.service /lib/systemd/system

echo " "
echo "Enable dbot sevice for upstart"
echo " "

sudo systemctl enable dbot.service