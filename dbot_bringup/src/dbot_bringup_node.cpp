#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <sensor_msgs/Joy.h>
#include <ros/duration.h>
#include <ros/time.h>
#include <math.h>

#include "dbot_msgs/RobotState.h"

ros::Subscriber state_sub;

dbot_msgs::RobotStatus robot_state_msg;

bool collect_request;
bool continue_collection = true;
std::string end_button_sym, collect_button_sym;
int end_button_num = 0, collect_button_num = 0;

void joy_CB(const sensor_msgs::Joy joy_msg)
{
	if(joy_msg.buttons[collect_button_num]==1)
	{
		collect_request = true;
	}
	else
	{
		collect_request = false;
	}

	if(joy_msg.buttons[end_button_num]==1)
	{
		continue_collection = false;
	}
}

int main(int argc, char** argv){
  ros::init(argc, argv, "dbot_bringup_node");

  ros::NodeHandle nh;

  state_sub = nh.subscribe("/robot_state", 0, SubRobotStatus);
  ros::Subscriber sub_joy = n.subscribe("/joy", 100, joy_CB);
  
  current_time = ros::Time::now();
  last_time = ros::Time::now();
  ros::Duration duration_min(1);

	// Get button numbers to collect waypoints and end collection
  ros::param::get("/outdoor_waypoint_nav/collect_button_num", collect_button_num);
  ros::param::get("/outdoor_waypoint_nav/end_button_num", end_button_num);

  ros::Rate r(20.0);
  while(nh.ok()){

    ros::spinOnce();               // check for incoming messages
    MotionAccControl(cmd_vel_msg, imu_msg);
    OdomPub(robot_status_msg);
    
    r.sleep();
  }
}