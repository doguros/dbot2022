/*
 *  Author : Kyung Yeop Han (yeop@dogu.xyz)
 */

#ifndef DBOT_COMMON_NODE_STATE_H
#define DBOT_COMMON_NODE_STATE_H
namespace dbot_common{

enum NodeState{
  IDLE,
  RUNNING,
  PAUSE,
  SUCCESS,
  FAILURE
};
} //namespace dbot_common

#endif //DBOT_NODE_STATE_H